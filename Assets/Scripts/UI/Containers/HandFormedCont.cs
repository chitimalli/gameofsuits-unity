using UnityEngine;
using System.Collections;

public class HandFormedCont : FFUIContainer {

	public UILabel 	handText;
	public UISprite	formedSprite;

	protected override void OnShow()
	{
		//SoundMgr.Instance.PlayUISound (Enums.UISoundID.UI_Woosh);
	}
	
	protected override void OnHide(){}
	
	public override void ShowContainer()
	{
		showTime = 0.5f;
		iTween.MoveTo(gameObject, iTween.Hash("position", showPos, "islocal", true, "time", 1.5f,"easetype","easeOutElastic"));
		IsShowing = true;
		OnShow ();
	}
	
	public override void HideContainer()
	{
		handText.text 		= "";
		formedSprite.enabled = false;

		iTween.MoveTo(gameObject, iTween.Hash("position", hidePos, "islocal", true, "time", hideTime,"easetype","linear"));
		IsShowing = false;
		OnHide ();
	}

	public void ShowContWithText(string txt)
	{
		formedSprite.enabled = true;

		handText.text = txt;
		ShowContainer ();
	}
}
