using UnityEngine;
using System.Collections;

public class TimerContainer : FFUIContainer {

	public UISprite		fillSpriteBkg;
	public UILabel   	gameTimerTxt;
	public UILabel		gameModeTxt;

	public UILabel		buffTimerTxt;

	public UIProgressBar bonusBar;

	
	int lastBuffTime = 0;
	int lastTime = 0;
	
	public int LastBuffTime {get{return lastBuffTime;} set{lastBuffTime = value;}}
	public int LastGameTime {get{return lastTime;} set{lastTime = value;}}


	public void UpdateGameTimer()
	{
		if(Game.Instance.GameMode == Enums.GAMEMODE.TIMED)
		{
			gameModeTxt.text = "sec";
			if(Game.Instance.IsFrozen)
			{
				gameTimerTxt.text =  ""+ Game.Instance.TimeLeft.ToString("0:00") +" + "+ Game.Instance.FreezeTimeLeft.ToString("0");
			}
			else
			{
				if(Game.Instance.TimeLeft<5.0f)
				{
					gameTimerTxt.color = Color.red;
					
					int currGameTime = (int)Game.Instance.TimeLeft;
					
					if(currGameTime != LastGameTime)
					{
						LastGameTime = currGameTime;
					}
					
				}

				
				if(Game.Instance.TimeLeft >= 0)
				{
					gameTimerTxt.text =  Game.Instance.TimeLeft.ToString("00");

					float timeLeftRation = Game.Instance.TimeLeft/60.0f;
					fillSpriteBkg.fillAmount = timeLeftRation;
				}
				
			}
			UpdateBuffTimer();
		}
		else if(Game.Instance.GameMode == Enums.GAMEMODE.MOVES)
		{
			gameModeTxt.text = "moves";
			gameTimerTxt.text =  Game.Instance.MovesLeft.ToString();
		}
		else if(Game.Instance.GameMode == Enums.GAMEMODE.UNLIMITED)
		{
			gameTimerTxt.text 	= "";
			gameModeTxt.text 	= "";
		}
	}

	public void UpdateBuffTimer()
	{
//		int buffInt = (int)Game.Instance.BuffTimeLeft;
//		string buffStr = "";
//
//		if(buffInt >2 && buffInt<3)	buffStr = "Get";
//		else if(buffInt >1 && buffInt<2) buffStr = "Set";
//		else if(buffInt >0 && buffInt<1) buffStr = "Go";
//
//		buffTimerTxt.text = buffStr;

		//Buff Time
		if(Game.Instance.BuffTimeLeft >= 0.25f)
		{
			buffTimerTxt.text 	=  Game.Instance.BuffTimeLeft.ToString("0");
			
			int currBuffTime = (int)Game.Instance.BuffTimeLeft;
			
			if(currBuffTime != lastBuffTime)
			{
				lastBuffTime = currBuffTime;
			}

		}else if (Game.Instance.BuffTimeLeft < 1.0f && Game.Instance.BuffTimeLeft > 0)
		{
			buffTimerTxt.text = "GO";
		}else
		{
			buffTimerTxt.text = "";
		}
	}

	public void UpdateBonusBar(float currBonus, float totalBonus)
	{
		//bonusTimerTxt.text = "+"+ totalBonus.ToString ("");

		//float bFullScale = bonusBarBkg.transform.localScale.x;
		//float bBarScale = (currBonus / totalBonus) * bFullScale;

		//Debug.Log("*** About to scale to : "+ bBarScale + "bonusTime : "+bTime);

		//bonusBar.transform.localScale = new Vector3 (bBarScale,bonusBar.transform.localScale.y,bonusBar.transform.localScale.z);
		//float bBarAddScale = ((bTime - 100) / bonusTime) * bFullScale;
		//bonusAddedBar.transform.localScale = new Vector3 (bBarAddScale,bonusAddedBar.transform.localScale.y,bonusAddedBar.transform.localScale.z);
	}

	public void Clean ()
	{
		LastBuffTime = 0;
		LastGameTime = 0;
	}
}
