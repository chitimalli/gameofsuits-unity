using UnityEngine;
using System.Collections;

public class CoinsWonContainer : FFUIContainer {

	public CoinPF coinsWon;
	public GameObject bkg;

	// Coin Multiple
	public TextMesh multipleTxt;
	public GameObject multipleBkg;


	public void OnGUI()
	{
		if(coinsWon != null)
		{
			coinsWon.UpdateBtnTxt(Game.Instance.CoinWonInGame.ToString());
		}
	}
}
