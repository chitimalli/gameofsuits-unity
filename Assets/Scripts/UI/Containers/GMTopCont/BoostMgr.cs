using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoostMgr : FFUIContainer {

	static BoostMgr m_Instance = null;
	public static BoostMgr Instance{get{ return m_Instance as BoostMgr;}}
	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
		LifeLeft 		= C.START_LIFE;
	}

	public List<BoostBtn> selectedBoost = new List<BoostBtn> ();

	public SelectedBoostUI selectedBoostScrollView;
	public PickupBoostUI pickupScrollView;

	int m_totalSwaps 	= 0;
	int m_totalZaps 	= 0;
	int m_totalFreeze 	= 0;
	int m_totalShuffle 	= 0;
	int m_totalZips 	= 0;
	int m_totalLife 	= 0;
	int m_Coins			= 0;
	
	public int SwapsLeft 		{get{return m_totalSwaps;} 		set{m_totalSwaps = value;}}
	public int ZapsLeft 		{get{return m_totalZaps;} 		set{m_totalZaps = value;}}
	public int ZipsLeft 		{get{return m_totalZips;} 		set{m_totalZips = value;}}
	public int FreezesLeft 		{get{return m_totalFreeze;} 	set{m_totalFreeze = value;}}
	public int ShufflesLeft 	{get{return m_totalShuffle;} 	set{m_totalShuffle = value;}}
	public int LifeLeft 		{get{return m_totalLife;} 		set{m_totalLife = value;}}
	public int Coins 			{get{return m_Coins;} 			set{m_Coins = value;}}

	protected override void OnShow ()
	{
		base.OnShow ();

		GameHeroCont.Instance.ShowContainer ();
		//GameHeroCont.Instance.SetPosNScale (showHeroPosScale);
		MMTopContainer.Instance.SetBoostSelectMode ();

		//playBtnsCont.ShowContainer ();
		pickupScrollView.ShowContainer ();
	}

	public void OnStartGame()
	{
		
		StartView.Instance.HideView ();
		GameView.Instance.ShowView ();
		MMTopContainer.Instance.HideContainer ();
		pickupScrollView.HideContainer ();
		//playBtnsCont.HideContainer ();
		selectedBoostScrollView.ShowContainer ();
		

		GameHeroCont.Instance.HideContainer ();
		
		StartCoroutine(C.DelayAction(1.0f,() => {
			Game.Instance.StartGame ();
		}));
	}

	public void TogglePauseBtn(bool show)
	{

	}

	public void InitLevelBoost(ProgressItem pItem)
	{
		if(pItem != null)
		{
			SwapsLeft 		= pItem.FreeSwaps;
			ZapsLeft 		= pItem.FreeZaps;
			FreezesLeft 	= pItem.FreeFreezes;
			ShufflesLeft	= pItem.FreeShuffles;
			ZipsLeft		= pItem.FreeZips;

			LifeLeft 		= C.START_LIFE;
			Coins			= C.START_COINS;
		}
	}

	
	public int BoostCount(Enums.BOOST _boost)
	{
		if(_boost == Enums.BOOST.SWAP)
			return SwapsLeft;
		else if(_boost == Enums.BOOST.BOMB)
			return FreezesLeft;
		else if(_boost == Enums.BOOST.SHUFFLE)
			return ShufflesLeft;
		else if(_boost == Enums.BOOST.ZAP)
			return ZapsLeft;
		if(_boost == Enums.BOOST.ZIP)
			return ZipsLeft;
		else if(_boost == Enums.BOOST.COIN)
			return Coins;
		else if(_boost == Enums.BOOST.LIFE)
			return LifeLeft;
		return 0;
	}

	public bool UpgardeBoost(Enums.BOOST _boost)
	{
		return false;
	}
	
	public bool BuyBoost(Enums.BOOST _boost)
	{
		if(_boost == Enums.BOOST.SWAP)
		{
			if(Coins >= 25)
			{
				Coins -= 25;
				SwapsLeft += 10;
				return true;
			}
		}
		
		if(_boost == Enums.BOOST.ZAP)
		{
			if(Coins >= 25)
			{
				Coins -= 25;
				ZapsLeft += 5;
				return true;
			}
		}

		if(_boost == Enums.BOOST.SHUFFLE)
		{
			if(Coins >= 25)
			{
				Coins -= 25;
				ShufflesLeft += 1;
				return true;
			}
		}
		
		if(_boost == Enums.BOOST.BOMB)
		{
			if(Coins >= 25)
			{
				Coins -= 25;
				FreezesLeft += 1;
				return true;
			}
		}

		if(_boost == Enums.BOOST.ZIP)
		{
			if(Coins >= 25)
			{
				Coins -= 25;
				ZipsLeft += 10;
				return true;
			}
		}
		
		if(_boost == Enums.BOOST.LIFE)
		{
			LifeLeft += 1; 
			return true;
		}
		
		return false;
	}
	
	public void UseBoost(Enums.BOOST _boost)
	{
		if(_boost == Enums.BOOST.SWAP && SwapsLeft >0)
			SwapsLeft -= 1;

		if(_boost == Enums.BOOST.ZIP && ZipsLeft >0)
			ZipsLeft -= 1;

		if(_boost == Enums.BOOST.ZAP && ZapsLeft >0)
			ZapsLeft -= 1;
		
		if(_boost == Enums.BOOST.BOMB && FreezesLeft >0)
			FreezesLeft -= 1;
		
		if(_boost == Enums.BOOST.LIFE && LifeLeft >0)
			LifeLeft -= 1;
		
	}

	public bool ApplySwapBoost(BaseBoardObject _objA,BaseBoardObject _objB, bool isFree = false, bool teleport = false)
	{
		bool retval = false;
		if(_objA != null && _objB != null && (SwapsLeft > 0 || isFree))
		{
			Vector3 _cardAPos = _objA.transform.position;
			int _cARow 	= _objA.RowID;
			int _cACol  = _objA.ColID;
			
			if(teleport)
				TeleportToPosition(_objA.gameObject,_objB.transform.position,0.5f);
			else
				SlideToPosition(_objA.gameObject,_objB.transform.position,0.25f);

			
			_objA.RowID = _objB.RowID;
			_objA.ColID = _objB.ColID;
			
			if(teleport)
				TeleportToPosition(_objB.gameObject,_cardAPos,0.5f);
			else
				SlideToPosition(_objB.gameObject,_cardAPos,0.5f);

			_objB.RowID = _cARow;
			_objB.ColID = _cACol;
			
			_objA.PosDelta = Vector2.zero;
			_objB.PosDelta = Vector2.zero;
			
				FeedbackContainer.Instance.ShowErrorFeedback("Swap");
				_objA.RotateOnce();
				_objB.RotateOnce();

				if(!isFree)
					UseBoost (Enums.BOOST.SWAP);

			PFLoader.Instance.ShowEffect(_objA.RowID, _objA.ColID,Enums.CFX.SLASH);
				_objA.PlaySound(Enums.ACTION_SND.SWAP,0);

			retval =  true;
		}else
		{
			FeedbackContainer.Instance.ShowErrorFeedback("NO >SWAPS< LEFT");
			retval = false;
		}
		
		GameBoard.Instance.ClearSelectedObjects();
		return retval;
	}
	
	public bool ApplyZapBoost()
	{
		if(ZapsLeft > 0)
		{
			FeedbackContainer.Instance.ShowErrorFeedback("ZAP Applied");
			UseBoost (Enums.BOOST.ZAP);
			return true;
		}else
		{
			FeedbackContainer.Instance.ShowErrorFeedback("NO >ZAPS< LEFT");
			//PopupMgr.Instance.ShowPopContainer (Enums.POPUP_TYPE.STORE, Enums.POPUP_TAB.ZAP);
			return false;
		}
	}

	public bool ApplyShufleBoost()
	{
		if(ShufflesLeft > 0)
		{
			FeedbackContainer.Instance.ShowErrorFeedback("Shuffle Applied");
			UseBoost (Enums.BOOST.SHUFFLE);
			return true;
		}else
		{
			FeedbackContainer.Instance.ShowErrorFeedback("NO >Shuffles< LEFT");
			return false;
		}
	}
	
	
	public void ShowLoot(HandData hand)
	{
		int qty = (int)hand.HandType;
		int lootID = Random.Range (0,(int)Enums.LootType.MAX_LOOT - 1);
		
		Enums.LootType loot = (Enums.LootType)lootID;
		
		LootObject lootObj 				= PFLoader.Instance.LootPrefabObj;
		
		if(lootObj != null && lootObj.CompareTag(C.LOOTOBJ))
		{
			switch(loot)
			{
			case Enums.LootType.COIN_LOOT:
			{
				Coins 	+= qty;
				break;
			}
			case Enums.LootType.FREEZE_LOOT:
			{
				FreezesLeft 	+= qty;
				break;
			}
			case Enums.LootType.HANDS_LOOT:
			{
				break;
			}
			case Enums.LootType.SWAP_LOOT:
			{
				SwapsLeft 	+= qty;
				break;
			}
			}
			
			lootObj.SetType(loot);
			SoundMgr.Instance.PlayLootSound (loot);
			lootObj.TossFromPos(new Vector2(3,3));
		}
	}



	Vector3  RandomCircle(Vector3 center, float radius)
	{
		// create random angle between 0 to 360 degrees
		float ang  = Random.value * 360.0f;
		Vector3 pos;
		pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
		pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
		pos.z = center.z;
		return pos;
	}
}
