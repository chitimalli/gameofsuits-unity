using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class FeedbackContainer : FFUIContainer {

	static FeedbackContainer m_Instance = null;
	public static FeedbackContainer Instance{get{ return m_Instance as FeedbackContainer;}}

	void Awake()
	{
		m_Instance = this;
	}

	public HandFormedCont 	handFormedCont;
	public HandFormedCont 	errorFormedCont;
	public UIWidget thisWidget;

	public void ResetFeedbackContainer()
	{
		if(GameBoard.Instance.SelectedObjects.Count>0)
			return;

		thisWidget.alpha = 0.0f;
		handFormedCont.HideContainer ();
		errorFormedCont.HideContainer();
	}
	
	public void ShowHandFormedInfo(HandData _hand)
	{
		thisWidget.alpha = 1.0f;

		handFormedCont.ShowContWithText (_hand.HandTypeString.ToUpper());
		SoundMgr.Instance.PlayHandSound (_hand);

		StartCoroutine(C.DelayAction(2.5f,()=>{
			handFormedCont.HideContainer();
		}));
	}

	public void ShowErrorFeedback(string _errStr)
	{
		thisWidget.alpha = 1.0f;

		errorFormedCont.ShowContWithText (_errStr);

		StartCoroutine(C.DelayAction(1.5f,()=>{
			errorFormedCont.HideContainer();
		}));
	}
}
