//#define GOT_PRIME31_GAMECENTER

using UnityEngine;
using System.Collections;

public class GMTopContainer: FFUIContainer 
{
	static GMTopContainer m_Instance = null;
	public static GMTopContainer Instance{get{ return m_Instance as GMTopContainer;}}

	public FFUIButton pauseBtn;

	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	protected override void OnShow()
	{
		BoostMgr.Instance.ShowContainer ();
		pauseBtn.ShowButton ();
	}

	protected override void OnHide()
	{
		//BoostContainer.Instance.HideContainer ();
	}

	public override void ShowContainer()
	{
		//TweenParms parms = new TweenParms().Prop("localPosition", showPos).Ease(EaseType.EaseOutQuart).AutoKill(true);
		//HOTween.To (gameObject.transform, _time, parms);
		transform.position 		= showPos;


		IsShowing = true;
		transform.localScale 	= showScale;

		OnShow ();
	}
	
	public override void HideContainer()
	{
		transform.position 		= hidePos;
		IsShowing = false;
		transform.localScale = hideScale;

		OnHide ();
	}
}