using UnityEngine;
using System.Collections;

public class GameInfoContainer : FFUIContainer {

	static GameInfoContainer m_Instance = null;
	public static GameInfoContainer Instance{get{ return m_Instance as GameInfoContainer;}}
	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	public TimerContainer  	timerContainer;
	public GameObject  		scoreCont;

	public GameObject 	handsIcon;

	// Score related
	public GameObject	scoreIcon;
	public UILabel   	scoreTxt;
	int currScore 		= 0;

	// XP related
	public GameObject	xpIcon;
	public UILabel   	xpTxt;


	int currXP;
	string scoreStr = "";
	int deltaScore  = 1;


	public override void ShowContainer()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", showPos, "islocal", true, "time", showTime,"easetype","easeInOutElastic"));
		IsShowing = true;
		OnShow ();

		Debug.Log (">>> Showing Game Info Container");
	}
	
	public override void HideContainer()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", hidePos, "islocal", true, "time", hideTime,"easetype","spring"));
		
		IsShowing = false;
		
		OnHide ();
	}

	public void UpdateScoreAndXP()
	{
		//Debug.Log ("** UpdateScoreAndXP>  " + currScore +   "  TotScore "+Game.Instance.TotalScore );
		if(currScore < Game.Instance.TotalScore)
		{
			int diff =  Game.Instance.TotalScore + currScore;

			currScore += (diff > deltaScore) ? deltaScore : diff;

			scoreStr 		= currScore.ToString("");
			scoreTxt.text 	= scoreStr;
			//xpTxt.text 		= Game.Instance.TotalXP + "";
		}
	}

	public void ShowScoreObjAt(int score)
	{
		ScoreObject scoreObj 				= PFLoader.Instance.ScorePrefabObj;
		
		if(scoreObj != null && scoreObj.CompareTag(C.SCOREOBJ))
		{
			scoreObj.SetTxt("+"+score.ToString());

			//scoreObj.transform.parent   			= scoreCont.transform;
			scoreObj.transform.position 			= new Vector3(scoreCont.transform.position.x,scoreCont.transform.position.y - 1.0f,-1);

			TeleportToPosition(scoreObj.gameObject,new Vector3(scoreCont.transform.position.x,scoreCont.transform.position.y,-1),5.0f);

			StartCoroutine(C.DelayAction(5.0f,()=>{
				Destroy(scoreObj);
			}));
		}
	}

	void OnGUI()
	{
		if(Game.Instance.IsFinished)
		{
			return;
		}
	
		if(timerContainer != null)
		{
			timerContainer.UpdateGameTimer();
		}
		
		UpdateScoreAndXP();
	}


	public void InitUI(Enums.GAMEMODE _mode)
	{
		//timerCont.InitUI (_mode);
		//handCont.SetActive (_mode == Enums.GAMEMODE.MOVES);
		//timerContainer.gameObject.SetActive (_mode == Enums.GAMEMODE.TIMED);// || _mode == Enums.GAMEMODE.RELAY);
	}

	public void Clean()
	{
		timerContainer.Clean ();
	}
}
