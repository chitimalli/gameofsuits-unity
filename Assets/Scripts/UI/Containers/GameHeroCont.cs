﻿using UnityEngine;
using System.Collections;

public class GameHeroCont : FFUIContainer {

	static GameHeroCont m_Instance = null;
	public static GameHeroCont Instance{get{ return m_Instance as GameHeroCont;}}

	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	public Hero goldenHero;
	public Hero darkHero;
	public Hero silverHero;
	public Hero multiHero;

	protected override void OnHide()
	{
		multiHero.StopRandAnim ();
	}

	public void MoveNScaleTo(Vector3 [] posNScale)
	{
		ShowContainer ();
		gameObject.MoveTo (posNScale [0], true, 0.5f, 0.5f, EaseType.easeInElastic);
		gameObject.ScaleTo (posNScale [1], 0.0f, 0.0f);
	}

	public void PlayAttack()
	{
		multiHero.PlayAnim ("attack");
	}

	public void PlayAnimation(string anim)
	{
		multiHero.PlayAnim (anim);
	}
}
