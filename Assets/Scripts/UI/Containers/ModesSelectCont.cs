using UnityEngine;
using System.Collections;

public class ModesSelectCont : FFUIContainer {

	//public LivesBtn 	livesBtn;
	public ModeSelectBtn[]	modesSelectBtns;
	public GameObject	heroBase;

	bool modeBtnsShowing = true;

	protected override void OnShow ()
	{
		base.OnShow ();

		//livesBtn.ShowButton ();
		ToggleModeSelectBtns ();
	}

	protected override void OnHide ()
	{
		base.OnHide ();
		
		//livesBtn.HideButton ();
		ToggleModeSelectBtns ();
	}

	public override void ShowContainer()
	{
		showTime = 1.0f;
		gameObject.MoveTo (showPos,true,showTime, 0.0f, EaseType.easeOutElastic);
		IsShowing = true;
		OnShow ();
	}
	
	public override void HideContainer()
	{
		gameObject.MoveTo (hidePos, true, hideTime, 0.0f, EaseType.easeInElastic);
		IsShowing = false;
		OnHide ();
	}

	void ToggleModeSelectBtns()
	{
		modeBtnsShowing 	= !modeBtnsShowing;
		heroBase.SetActive(modeBtnsShowing);

		foreach(ModeSelectBtn modeBtn in modesSelectBtns)
		{
			if(modeBtnsShowing)
			{
				modeBtn.ShowButton();
			}
			else
			{
				modeBtn.HideButton();
			}
		}
	}
}
