//#define GOT_PRIME31_GAMECENTER

using UnityEngine;
using System.Collections;


public class FFUIContainer: AnimBehaviour
{
	public Vector3 showPos;
	public Vector3 hidePos;

	public Vector3 showScale = Vector3.one;
	public Vector3 hideScale = Vector3.one;

	public float showTime  	= 0.15f;
	public float showDelay  = 0.0f;

	public float hideTime  	= 0.15f;
	public float hideDelay  = 0.0f;



	public bool IsShowing = false;

	protected virtual void OnShow()
	{
		//SoundMgr.Instance.PlayUISound (Enums.UISoundID.UI_Woosh);
	}

	protected virtual void OnHide(){}

	public virtual void ShowContainer()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", showPos, "scale", showScale, "islocal", true, "time", showTime,"easetype","spring"));

		IsShowing = true;
		OnShow ();
	}
	
	public virtual void HideContainer()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", hidePos, "scale", hideScale, "islocal", true, "time", hideTime));

		IsShowing = false;
		
		OnHide ();
	}
}