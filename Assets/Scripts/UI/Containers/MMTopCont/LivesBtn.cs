//#define GOT_PRIME31_GAMECENTER

using UnityEngine;
using System.Collections;

public class LivesBtn: BoostBtn 
{
	//public UILabel nextFillTxt;

	float FillTime = (float)C.LIFE_FILLRATE;

	void Update () 
	{
		FillTime -= Time.deltaTime;
		if(FillTime <= 0)
		{
			FillTime = (float)C.LIFE_FILLRATE;
			BoostMgr.Instance.LifeLeft += 1;
		}
	}

	public override void UpdateBtnTxt(string str = "")
	{
		if(btnTxt != null)
		{
			btnTxt.text = BoostMgr.Instance.LifeLeft.ToString();
		}
		
		if(btnShwTxt != null)
		{
			btnShwTxt.text = BoostMgr.Instance.LifeLeft.ToString();;
		}
	}

}