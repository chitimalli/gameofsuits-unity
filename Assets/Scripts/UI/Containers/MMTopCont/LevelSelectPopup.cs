using UnityEngine;
using System.Collections;

public class LevelSelectPopup : PopupTypeContainer 
{
	void SetMode()
	{
		GameView.Instance.SetUpUI (StartView.Instance.CurrPItem);

		BoostMgr.Instance.InitLevelBoost(StartView.Instance.CurrPItem);
		BoostMgr.Instance.ShowContainer ();
	}

	public override TabContainer ShowTabWithTabID(Enums.POPUP_TAB tabID)
	{
		TabContainer showingTab = base.ShowTabWithTabID (tabID);

		if(showingTab != null)
		{
			// Set the Tab ready
			if(showingTab.tabType == Enums.POPUP_TAB.START)
			{
				SetMode();
			}
			else if(showingTab.tabType == Enums.POPUP_TAB.MULLIGAN)
			{
				//((MulliganTab)showingTab).SetTab();
			}
			else if(showingTab.tabType == Enums.POPUP_TAB.RESULTS)
			{
				//((ResultsTab)showingTab).SetTab(()=>{GoHomeAction();},()=>{StartGameAction();});
			}
		}

		return null;
	}

	// Buttons Actions

	void StartGameAction()
	{
		StartView.Instance.HideView ();
		GameView.Instance.ShowView ();
		
		PopupMgr.Instance.PopFromTop();
		
		MMTopContainer.Instance.HideContainer ();
		GMTopContainer.Instance.ShowContainer ();
		
		StartCoroutine(C.DelayAction(1.0f,() => {
			Game.Instance.StartGame ();}));
	}
	
	void ResumeGameAction()
	{
		PopupMgr.Instance.PopFromTop();
		Game.Instance.SetGamePause (false);
	}

	void  GoHomeAction()
	{
		Game.Instance.IsFinished = true;
		GameBoard.Instance.ClearSelectedObjects ();
	

		PopupMgr.Instance.PopAll();
		StartView.Instance.SetHomeMode ();
	}
}
