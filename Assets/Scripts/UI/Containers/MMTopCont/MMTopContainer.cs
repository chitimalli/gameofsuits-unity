//#define GOT_PRIME31_GAMECENTER

using UnityEngine;
using System.Collections;

public class MMTopContainer: FFUIContainer 
{
	static MMTopContainer m_Instance = null;
	public static MMTopContainer Instance{get{ return m_Instance as MMTopContainer;}}
	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	public GameTitleCont 		gameTitle;
	public ModesSelectCont		modeselectCont;

	protected override void OnShow ()
	{
		base.OnShow ();

		modeselectCont.enabled 	= IsShowing;
		modeselectCont.ShowContainer ();

		gameTitle.enabled		= IsShowing;
	}

	protected override void OnHide ()
	{
		base.OnHide ();
		modeselectCont.enabled = IsShowing;
		gameTitle.enabled		= IsShowing;



	}

	public void SetBoostSelectMode()
	{
		gameTitle.HideContainer ();
		modeselectCont.HideContainer ();
	}
}