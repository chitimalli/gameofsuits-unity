﻿using UnityEngine;
using System.Collections;

public class ModeSelectBtn : ProgressItem {

	public BaseBoardObject bbo;

	protected override void OnClickAction()
	{
		base.OnClickAction ();
		ShowBoostSelection ();
	}

	public void ShowBoostSelection()
	{
		StartView.Instance.CurrPItem = this;
		diff = Enums.DIFFICULTY.HARD;
		UpdateGameSetings ();

		// Now let
		BoostMgr.Instance.ShowContainer ();
	}

	public void ShowProgressionMap()
	{

	}
}
