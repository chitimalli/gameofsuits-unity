using UnityEngine;
using System.Collections;

public class GameTitleCont : FFUIContainer {

	public override void ShowContainer()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", showPos, "islocal", true, "time", showTime,"easetype","easeInOutElastic"));
		IsShowing = true;
		OnShow ();
	}
	
	public override void HideContainer()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", hidePos, "islocal", true, "time", hideTime,"easetype","spring"));

		IsShowing = false;
		
		OnHide ();
	}
}