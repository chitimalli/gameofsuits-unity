﻿using UnityEngine;
using System.Collections;

public class FFUIButton : UIButton {

	public TextMesh btnTxt;
	public TextMesh btnShwTxt;

	public Vector3 showPos;
	public Vector3 hidePos;
	public float showTime;
	public float hideTime;

	public Vector3 showScale = Vector3.one;
	public Vector3 hideScale = Vector3.one;

	protected virtual void OnShow(){}
	protected virtual void OnHide(){}

	public virtual void UpdateBtnTxt(string str = "")
	{
		if(btnTxt != null)
		{
			btnTxt.text = str;
		}

		if(btnShwTxt != null)
		{
			btnShwTxt.text = str;
		}
	}

//	protected virtual void OnSelectAction()
//	{
//		if(playSelSound)
//			SoundMgr.Instance.PlayUISound (Enums.UISoundID.UI_Click);
//
//		if(scaleOnSel)
//			transform.localScale += new Vector3(0.2F, 0.2F, 0);
//
//		if(selectCallBack != null)
//			selectCallBack();
//	}
//
	protected virtual void OnClickAction(){}
//
//	public void ExecuteAction()
//	{
//		ToggleButton ();
//
//		if(IsSelected)
//		{
//			OnSelectAction();
//		}else
//		{
//			OnDeselectAction();
//		}
//	}

//	public virtual void ShowButton()
//	{
//		Anim.SpringContainer (gameObject, hidePos, showPos,hideScale,showScale, showTime);
//		OnShow ();
//	}
//	
//	public virtual void HideButton()
//	{
//		Anim.SpringContainer (gameObject, showPos, hidePos,showScale,hideScale, hideTime);
//		OnHide ();
//	}

	public virtual void ShowButton()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", showPos, "islocal", true, "time", showTime,"easetype","easeOutElastic"));
		OnShow ();
	}
	
	public virtual void HideButton()
	{
		iTween.MoveTo(gameObject, iTween.Hash("position", hidePos, "islocal", true, "time", hideTime,"easetype","easeInElastic"));
		OnHide ();
	}
}
