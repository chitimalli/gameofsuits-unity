using UnityEngine;
using System.Collections;

public class BoostBtn : FFUIButton {

	public Enums.BOOST 		boostID;
	public Enums.POPUP_TAB	popTab;

	public override void UpdateBtnTxt(string str = "")
	{
		if(btnTxt != null )
		{
			string boostQt = BoostMgr.Instance.BoostCount(boostID).ToString();
			base.UpdateBtnTxt(boostQt);
		}
	}

	void Update()
	{
		UpdateBtnTxt ();
	}

	public void Init(Enums.BOOST _boostID)
	{
		boostID = _boostID;
	}


	protected override void OnClickAction()
	{
		base.OnClickAction ();

		switch(boostID)
		{
			case Enums.BOOST.BOMB:
			case Enums.BOOST.ZAP:
			case Enums.BOOST.SWAP:
			case Enums.BOOST.LIFE:
			case Enums.BOOST.SHUFFLE:
			case Enums.BOOST.ZIP:
			{
				//PopupMgr.Instance.ShowPopContainer (Enums.POPUP_TYPE.BOOST,popTab);
				if(BoostMgr.Instance.BoostCount(boostID) <= 0)
					BoostMgr.Instance.BuyBoost(boostID);
				else 
					FeedbackContainer.Instance.ShowErrorFeedback(boostID.ToString());

				break;
			}
			case Enums.BOOST.COIN:
			{
				//PopupMgr.Instance.ShowPopContainer (Enums.POPUP_TYPE.STORE, Enums.POPUP_TAB.COINS);
				break;
			}
		}
	}
}