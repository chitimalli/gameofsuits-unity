//#define GOT_PRIME31_GAMECENTER

using UnityEngine;
using System.Collections;

public class StartView: BaseView 
{
	public static StartView Instance = null;

	void Awake()
	{
		DontDestroyOnLoad(this);
		Instance = this;
	}

	ProgressItem currItem;
	public ModesSelectCont 	ModeSelectCont;
	public GameTitleCont	gameTitleCont;

	public Vector3[] showHeroPosScale;

	public ProgressItem CurrPItem{get{return currItem;} set{currItem = value;}}


	protected override void OnShow ()
	{
		base.OnShow ();

		ModeSelectCont.ShowContainer ();
		gameTitleCont.ShowContainer ();
		//BoostMgr.Instance.HideContainer ();
		//heroHolderBtn.ShowButton();

		GameHeroCont.Instance.MoveNScaleTo (showHeroPosScale);
	}

	protected override void OnHide ()
	{
		ModeSelectCont.HideContainer ();
		gameTitleCont.HideContainer ();

		//heroHolderBtn.HideButton();
	}

	public void SetHomeMode()
	{
		ShowView();
		
		GameView.Instance.HideView ();
		//PopupMgr.Instance.HideContainer ();
		//BoostMgr.Instance.HideContainer ();
		//MMTopContainer.Instance.SetHomeMode ();
		ModeSelectCont.ShowContainer ();
		gameTitleCont.ShowContainer ();

		//heroHolderBtn.ShowButton();
	}
}