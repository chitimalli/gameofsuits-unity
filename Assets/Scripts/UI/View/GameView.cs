using UnityEngine;
using System.Collections;

public class GameView : BaseView {

	static GameView m_Instance = null;
	public static GameView Instance{get{ return m_Instance as GameView;}}
	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	public Vector3[] showHeroPosScale;

	public GameInfoContainer	gameInfoContianer;

	public void SetUpUI(ProgressItem pItem)
	{
		gameInfoContianer.InitUI (pItem.GameMode);
		BoostMgr.Instance.InitLevelBoost (pItem);
		FeedbackContainer.Instance.ResetFeedbackContainer ();
	}

	protected override void OnShow()
	{
		BoostMgr.Instance.ShowContainer ();
		gameInfoContianer.ShowContainer ();
		FeedbackContainer.Instance.HideContainer();
		GameBoard.Instance.ShowContainer ();

		GameHeroCont.Instance.MoveNScaleTo (showHeroPosScale);
	}

	protected override void OnHide()
	{
		//GMTopContainer.Instance.HideContainer();
		gameInfoContianer.HideContainer ();
		GameBoard.Instance.HideContainer ();
	}
}
