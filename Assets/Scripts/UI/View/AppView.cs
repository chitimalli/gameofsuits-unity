using UnityEngine;
using System.Collections;

public class AppView : MonoBehaviour {
	static AppView m_Instance;
	public GameHeroCont	heroHolderBtn;

	public static AppView Instance{get{ return m_Instance as AppView;}}

	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	Enums.DIRECTION	m_swipeDir = Enums.DIRECTION.NEWS;

	bool IsDrag;

	public Enums.DIRECTION SwipeDir {get{return m_swipeDir;} set{m_swipeDir = value;}}

	void Start()
	{
		StartCoroutine(C.DelayAction(0.5f,()=>{

			StartView.Instance.SetHomeMode();
		}));
	}

	// Update is called once per frame
	void Update () 
	{
		//#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WINRT
		//		if(Input.touchCount == 1)
		//		{
		//			GameMenu.Instance.ResetFeedbackContainer();
		//			HideConnector(false);
		//		}
		//
		//		if(Input.touchCount == 0)
		//		{
		//			Game.Instance.ValidateAndClearSelected ();
		//		}
		//#endif
		
		// TODO: Check for perfomance later
		//#if UNITY_WEBPLAYER || UNITY_STANDALONE || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
		if (Input.GetMouseButtonDown(0))
		{
			//touchDownStart 		= Input.mousePosition;
//			touchDragStart		= touchDownStart;
//
//			//touchDownEnd 		= Vector2.zero;
//			deltaDrag			= Vector2.zero;

			CheckForTouch(false);
		}
		
		if (Input.GetMouseButtonUp(0))
		{
			CheckForTouch(true);
		}

	}
	
	void FixedUpdate()
	{
		CheckForCardDrag();
		//CheckForProgressionDrag ();
	}



	Enums.DIRECTION ComputeDirection(Vector2 deltaV2, float delta)
	{
		if(deltaV2.x > delta)
			return Enums.DIRECTION.EAST;
		else if(deltaV2.x <= -delta )
			return Enums.DIRECTION.WEST;
		else if(deltaV2.y > delta)
			return Enums.DIRECTION.NORTH;
		else //if(deltaDrag.y < 0)
			return Enums.DIRECTION.SOUTH;
	}

	void CheckForTouch(bool isTouchUp)
	{
		if(GameBoard.Instance.DisableTouch)
			return;

		IsDrag = !isTouchUp;

		//ProgressionMap pMap = ProgressionMap.Instance;
		//CheckForBtnClick();

		if(isTouchUp && (!Game.Instance.IsFinished && !Game.Instance.IsPaused))
		{
			GameBoard.Instance.ToggleConnector(true);

			if(GameBoard.Instance.SelectedObjects.Count > 1)
			{
				GameBoard.Instance.ValidateAndClearSelected ();
				Game.Instance.CheckAndNotifGameOver ();
			}else if(GameBoard.Instance.SelectedObjects.Count == 1)
			{
				BaseBoardObject _obj = GameBoard.Instance.SelectedObjects[0];
				GameBoard.Instance.CheckForZapObject(_obj);
				
				GameBoard.Instance.ClearSelectedObjects();
			}
		}
		else
		{
			GameBoard.Instance.ToggleConnector(false);
		}
	}

	void CheckForCardDrag()
	{
		if(GameBoard.Instance.DisableTouch)
			return;

		if(!IsDrag)
			return;

		// drag match mode
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint( Input.mousePosition), Vector2.zero);
		if(hit.collider != null)
		{
			// GameBoard Related
			BaseBoardObject cardP = hit.collider.GetComponent<BaseBoardObject>();
			if (cardP)
			{
				if(!cardP.IsSelected)
				{
					GameBoard.Instance.ValidateAndAddToSelected(cardP);
				}else
				{
					GameBoard.Instance.CheckForbackTrack(cardP);
				}
			}
		}
	}

//	void CheckForBtnClick()
//	{
//		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
//		if(hit.collider != null)
//		{
//			SuitsButton pItem = hit.collider.GetComponent<SuitsButton>();
//			if(pItem)
//			{
//				//Debug.Log("*** Btn Click");
//				pItem.ExecuteAction();
//			}
//		}
//	}
}
