﻿using UnityEngine;
using System.Collections;

public abstract class BaseView : MonoBehaviour {

	public Vector3 showPos;
	public Vector3 hidePos;
	public bool IsShowing = false;

	protected virtual void OnShow(){}
	protected virtual void OnHide(){}


	public void ShowView(float _time = 0.5f)
	{
		//TweenParms parms = new TweenParms().Prop("localPosition", showPos).Ease(EaseType.EaseOutQuart).AutoKill(true);
		//HOTween.To (gameObject.transform, _time, parms);

		transform.position 		= showPos;
		IsShowing = true;
		OnShow ();
	}
	
	public void HideView(float _time = 0.5f)
	{
		//TweenParms parms = new TweenParms().Prop("localPosition", hidePos).Ease(EaseType.EaseOutQuart).AutoKill(true);
		//HOTween.To (gameObject.transform, _time, parms);
		transform.position 		= hidePos;

		IsShowing = false;
		OnHide ();
	}
}
