﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hero : MonoBehaviour {

	public string[] RandAnims;

	public void PlayAnim(string anim)
	{
		animation.Play (anim);
	}

	public void PlayRandAnim()
	{
		string RandAnim = RandAnims[Random.Range(0,RandAnims.Length)];

		if(RandAnim != null)
		{
			animation.Play (RandAnim);
		}

		StartCoroutine(C.DelayAction(5.0f,()=>{
			PlayRandAnim(); //4
		}));
	}

	public void StopRandAnim()
	{
		StopAllCoroutines ();
	} 
}
