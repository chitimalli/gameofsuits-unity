using UnityEngine;
using System.Collections;

public class PickupBoostUI : FFUIContainer {

	public UIScrollView scrollView;
	public UIScrollBar	scrollBar;
	public UIGrid		scrollGrid;

	public UIButton		closeBtn;
	public UILabel		titleLable;

	public FFUIContainer playBtnsCont;
	public Vector3[] showHeroPosScale;


	protected override void OnShow ()
	{
		base.OnShow ();
		
		GameHeroCont.Instance.ShowContainer ();
		GameHeroCont.Instance.MoveNScaleTo (showHeroPosScale);

		playBtnsCont.ShowContainer ();
	}
}
