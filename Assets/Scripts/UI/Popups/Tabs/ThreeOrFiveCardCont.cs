using UnityEngine;

public class ThreeOrFiveCardCont : FFUIContainer {

	public GameObject threeCardIcon;
	public GameObject fiveCardIcon;

	public TextMesh threeCardInfo;
	public TextMesh fiveCardInfo;

	public void UpdateInfo(Enums.GAMEMODE mode, string text)
	{
		threeCardIcon.SetActive(mode == Enums.GAMEMODE.TIMED);
		fiveCardIcon.SetActive(mode == Enums.GAMEMODE.MOVES);

		if(threeCardInfo != null) threeCardInfo.text 		= text;
		if(fiveCardInfo != null) fiveCardInfo.text	= text;
	}
}
