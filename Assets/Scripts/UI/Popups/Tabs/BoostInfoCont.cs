﻿using UnityEngine;
using System.Collections;

public class BoostInfoCont : TabContainer {

	public Enums.BOOST	 boostID;

	public GameObject 	boostIcon;
	public TextMesh 	boostName;
	public TextMesh 	boostDesc;

	protected override void OnShow()
	{

	}
	
	protected override void OnHide()
	{

	}
}