//#define GOT_PRIME31_GAMECENTER

using UnityEngine;
using System.Collections;

public class MulliganUI: PopupTypeContainer 
{
	public void OnUseMulligan()
	{
		if(Game.Instance.UseMulligan ())
		{
			PopupMgr.Instance.PopFromTop ();
			Game.Instance.SetGamePause(false);
		}else
		{
			// Cannot use Mulligan, probably out for funds
			//PopupMgr.Instance.ShowPopContainer (Enums.POPUP_TYPE.STORE, Enums.POPUP_TAB.COINS);
		}
	}

	public void OnGiveUp()
	{
		// We want to show the Results tab
		//PopupMgr.Instance.PopFromTop ();
		//PopupMgr.Instance.ShowPopContainer (Enums.POPUP_TYPE.LEVEL, Enums.POPUP_TAB.RESULTS);
	}
}