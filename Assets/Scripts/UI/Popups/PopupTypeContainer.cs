using UnityEngine;
using System.Collections;

public class PopupTypeContainer : FFUIContainer {

	public Enums.POPUP_TYPE popType;
	public TabContainer[] tabs;

	public Enums.POPUP_TAB showingTab;

	public override void ShowContainer()
	{
		//Debug.Log ("Showing Pop >>> "+popType);

		gameObject.transform.localScale = showScale;
		SlideToPosition (gameObject, showPos, showTime);
		IsShowing = true;
		OnShow ();
	}
	
	public override void HideContainer()
	{
		//Debug.Log ("Hiding Pop >>> "+popType);


		gameObject.transform.localScale = hideScale;
		SlideToPosition (gameObject, hidePos,hideTime);
		IsShowing = false;
		OnHide ();

		// Lets hide all the inner Tabs as well
		foreach (TabContainer tab in tabs)
		{
			tab.HideContainer();
		}
	}

	public virtual TabContainer ShowTabWithTabID(Enums.POPUP_TAB tabID)
	{
		if(!IsShowing)
			ShowContainer ();


		TabContainer retTab = null;
		
		foreach (TabContainer tab in tabs)
		{
			Debug.Log (">>> **  "+tabID +  " TabType: "+tab.tabType);

			if(tab.tabType == tabID)
			{
				if(!tab.IsShowing)
				{
					//Debug.Log("** Showing tab ** 3 "+ tab.tabType);
					tab.ShowContainer();// (tabID);
					retTab =  tab;
				}
			}
			else
			{
				//if(tab.IsShowing)
				//Debug.Log("** Hiding tab ** 4 "+ tab.tabType);
					tab.HideContainer();
			}
		}

		return retTab;
	}
}
