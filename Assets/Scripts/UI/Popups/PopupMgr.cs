using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PopupMgr : FFUIContainer {

	static PopupMgr m_Instance = null;
	public static PopupMgr Instance{get{ return m_Instance as PopupMgr;}}
	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	List<PopupTypeContainer>  showingPopups = new List<PopupTypeContainer>();

	public FFUIButton 				DimBkgBtn;
	public PopupTypeContainer[]  	PopupTypes;


	public bool IsShowingPop{get{return showingPopups.Count > 0;}}


//	protected override void OnShow()
//	{
//		base.OnShow ();
//		//Pause the game, if we are playing
//		if(GameView.Instance.IsShowing
//		   && Game.Instance.IsStarted 
//		   && !Game.Instance.IsFinished)
//		{
//			Game.Instance.SetGamePause(true);
//		}
//
//
//		// We dont want all the Pops to toggle when clicked on Dim bkg
//		if(DimBkgBtn != null )
//		{
//			DimBkgBtn.deselectCallBack = ()=>{PopFromTop();};
//		}
//	}
//	
//	protected override void OnHide()
//	{
//		//Resume the game, is paused already
//		if(GameView.Instance.IsShowing
//		   && Game.Instance.IsStarted 
//		   && !Game.Instance.IsFinished)
//		{
//			Game.Instance.SetGamePause(false);
//		}
//		else if(StartView.Instance.IsShowing)
//		{
//			//StartView.Instance.ProgressMap.ShowContainer ();
//			MMTopContainer.Instance.SetHomeMode ();
//		}
//
//		if(DimBkgBtn != null)
//			DimBkgBtn.HideButton();
//	}

	public void PopAll()
	{
		while(showingPopups.Count <= 0)
		{
			PopFromTop ();
		}
	}

	public void PopFromTop()
	{
		if(showingPopups.Count > 0)
		{
			PopupTypeContainer topShowing = showingPopups [showingPopups.Count - 1];

			if(topShowing != null)
			{
				topShowing.HideContainer();
				showingPopups.Remove(topShowing);
			}
		}

		if(showingPopups.Count > 0)
		{
			FFUIContainer topShowing = showingPopups [showingPopups.Count - 1];
			if(topShowing != null)
			{
				topShowing.ShowContainer();
			}
		}

		if(showingPopups.Count <= 0)
		{
			if(DimBkgBtn != null)
			{
				DimBkgBtn.HideButton();
			}

			//HideContainer();
		}
	}

	void PushToTop(PopupTypeContainer popup)
	{
		if(popup != null && !showingPopups.Contains(popup))
		{
			if(showingPopups.Count > 0)
			{
				PopupTypeContainer topShowing = showingPopups [showingPopups.Count - 1];
				if(topShowing != null)
				{
					topShowing.HideContainer();
				}
			}

			showingPopups.Add (popup);
			popup.ShowContainer();
		}else
		{
			// Bring that Pop to Top
			Debug.Log ("Push to the TOP");
		}

		if(showingPopups.Count > 0)
		{
			if(DimBkgBtn != null)
			{
				DimBkgBtn.ShowButton();
			}

//			if(!IsShowing)
//				ShowContainer();
		}
	}

	public void ShowPopContainer(Enums.POPUP_TYPE popTypeID , Enums.POPUP_TAB tabID, bool toggle = false)
	{
//		if(!IsShowing)
//			ShowContainer ();

		foreach(PopupTypeContainer poptype in PopupTypes)
		{
			if(poptype.popType == popTypeID)
			{
				//Debug.Log("Showing >> 1 >> "+ popTypeID  +" >> "+tabID + "  showing >> "+poptype.IsShowing);

				if(!poptype.IsShowing)
				{
					//Debug.Log("Showing >> 1 >> "+ popTypeID  +" >> "+tabID);
					PushToTop (poptype);
					poptype.ShowTabWithTabID (tabID);
				}
				else if(toggle)
				{
					PopFromTop();
				}
			}
			// hide every other Pop
			else if(poptype.IsShowing)
			{
				poptype.HideContainer();
			}
		}
	}
}
