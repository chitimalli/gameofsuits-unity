//#define GOT_PRIME31_GAMECENTER

using UnityEngine;
using System.Collections;

public class TabContainer: FFUIContainer 
{
	public Enums.POPUP_TAB 	tabType;

	public string titleString = "";

	void Start()
	{
		HideContainer ();
	}

	public override void ShowContainer()
	{
		SlideToPosition (gameObject, showPos, showTime);
		IsShowing = true;
		OnShow ();
	}
	
	public override void HideContainer()
	{
		SlideToPosition (gameObject, hidePos,hideTime);
		IsShowing = false;
		OnHide ();
	}
}