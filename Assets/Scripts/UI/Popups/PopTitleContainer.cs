using UnityEngine;
using System.Collections;

public class PopTitleContainer : FFUIContainer {

	public FFUIButton 		closeBtn;
	public TextMesh 		popTitle;
	public GameObject		background;


	protected override void OnShow()
	{
		//closeBtn.deselectCallBack = () => {GUIPopupMgr.Instance.PopFromTop();};
	}

	public void SetUpTitleInfo(string _popTitle = "", bool showClose = true, bool showCoins = true, bool showBkg = true)
	{
		if(popTitle != null)
		{
			popTitle.text = _popTitle;
		}
	}
}
