﻿using UnityEngine;
using System.Collections;

public class SoundMgr : MonoBehaviour {

	public static SoundMgr Instance ;
	
	void Awake()
	{
		DontDestroyOnLoad(this);
		Instance = this;
	}

	public AudioClip[] m_SelectSounds;
	public AudioClip[] m_LootSounds;
	public AudioClip[] m_HandSounds;
	public AudioClip[] m_MiniHandSounds;
	public AudioClip[] m_VoiceFeedbackSounds;

	public AudioClip[] m_UISounds;
	public AudioClip[] m_Music;

	public AudioClip[] m_cardsPlace; 	//4
	public AudioClip[] m_cardsShove; 	//4
	public AudioClip[] m_cardsSlide; 	//8
	public AudioClip[] m_cardsShuffle; 	//3

	public AudioClip[] m_coinsDrop; 	//1
	public AudioClip[] m_coinsSelect; 	//7

	
	public AudioClip[] m_chipsLay; 		//3
	public AudioClip[] m_chipsCollide; 	//4
	public AudioClip[] m_chipsHandle; 	//6
	public AudioClip[] m_chipsStack; 	//6

	public AudioClip[] m_ZAPObjSound;
	public AudioClip[] m_BombObjSound;


	public GameObject   m_AudioClipObjdPrefab;
	public AudioClipObject AudioClipPrefab		{get{return (Instantiate(m_AudioClipObjdPrefab) as GameObject).GetComponent<AudioClipObject>();}}

	float audioKillTime = 3.0f;

	// 0=highest priority, 255=lowest priority.

	public void PlaySelectSound(int sID,float _vol = 1.0f, float _pit = 1.0f)
	{
		if(sID >= m_SelectSounds.Length)
			sID = m_SelectSounds.Length - 1;
		
		PlayClip(m_SelectSounds[0],_vol,_pit);
	}
	
	public void PlayLootSound(Enums.LootType _type,float _vol = 1.0f, float _pit = 1.0f)
	{
		int lootID = (int)_type;

		if(lootID >= m_LootSounds.Length)
			lootID = m_LootSounds.Length - 1;

		PlayClip(m_LootSounds[lootID],_vol,_pit);
	}
	
	public void PlayUISound(Enums.UISoundID _sID,float _vol = 1.0f, float _pit = 1.0f)
	{
		int sID = (int)_sID;

		if(sID >= m_UISounds.Length)
			sID = m_UISounds.Length - 1;

		PlayClip(m_UISounds[sID],_vol,_pit);
	}

	public void PlayHandSound(HandData _hand,float _vol = 1.0f, float _pit = 1.0f)
	{
		int sID = 0;
		AudioClip aClip = null;

		if(Game.Instance.MiniHandValid && _hand.IsMini && _hand.MiniHandType != Enums.MHTYPE.INVALID)
		{
			sID = (int)_hand.MiniHandType;

			//Debug.Log("*** sID "+sID + " MinHandType "+_hand.MiniHandType);

			if(sID >= m_MiniHandSounds.Length)
				sID = m_MiniHandSounds.Length - 1;

			aClip = m_MiniHandSounds[sID];
		}else
		{
			sID = (int)_hand.HandType;
			//Debug.Log("*** sID "+sID + " HandType "+_hand.HandType);


			if(sID >= m_HandSounds.Length)
				sID = m_HandSounds.Length - 1;
			
			aClip = m_HandSounds[sID];
		}

		PlayClip(aClip,_vol,_pit);
	}

	public void PlayVoiceFeedback(Enums.FEEDBACK_SND _sID,float _vol = 1.0f, float _pit = 1.0f)
	{
		int sID = (int)_sID;

		if(sID >= m_VoiceFeedbackSounds.Length)
			sID = m_VoiceFeedbackSounds.Length - 1;

		PlayClip(m_VoiceFeedbackSounds[sID],_vol,_pit);
	}

	public void PlayCardSound(Enums.CARD_SND sndType,int id,float _vol = 1.0f, float _pit = 1.0f)
	{
		AudioClip[] cSndArray = null;

		if(sndType == Enums.CARD_SND.PLACE)
			cSndArray = m_cardsPlace;
		else if(sndType == Enums.CARD_SND.SHOVE)
			cSndArray = m_cardsShove;
		else if(sndType == Enums.CARD_SND.SHUFFLE)
			cSndArray = m_cardsShuffle;
		else if(sndType == Enums.CARD_SND.SLIDE)
			cSndArray = m_cardsSlide;

		if(id >= cSndArray.Length)
			id = cSndArray.Length - 1;

		PlayClip(cSndArray[id],_vol,_pit);
	}

	public void PlayChipSound(Enums.CHIP_SND sndType,int id,float _vol = 1.0f, float _pit = 1.0f)
	{
		AudioClip[] cSndArray = null;
		
		if(sndType == Enums.CHIP_SND.LAY)
			cSndArray = m_chipsLay;
		else if(sndType == Enums.CHIP_SND.COLLIDE)
			cSndArray = m_chipsCollide;
		else if(sndType == Enums.CHIP_SND.HANDLE)
			cSndArray = m_chipsHandle;
		else if(sndType == Enums.CHIP_SND.STACK)
			cSndArray = m_chipsStack;
		
		if(id >= cSndArray.Length)
			id = cSndArray.Length - 1;
		
		PlayClip(cSndArray[id],_vol,_pit);
	}

	void PlayClip(AudioClip aClip,float _vol = 1.0f, float _pit = 1.0f)
	{
		AudioClipObject aClipObj 	= AudioClipPrefab;

		if(aClip != null && aClipObj != null)
		{
			aClipObj.AssignClipAndPlay(aClip, 140,_vol,_pit);
			aClipObj.transform.parent = transform;
			
			Destroy(aClipObj.gameObject,audioKillTime);
		}
	}

}
