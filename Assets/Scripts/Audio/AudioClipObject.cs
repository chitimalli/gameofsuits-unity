﻿using UnityEngine;
using System.Collections;

public class AudioClipObject : MonoBehaviour {


	public void AssignClipAndPlay(AudioClip _clip, int _prio = 128, float _vol = 1.0f, float _pit = 1.0f)
	{
		if(_clip != null && audio != null)
		{
			audio.clip 		= _clip;
			audio.priority 	= _prio;
			audio.volume	= _vol;
			audio.pitch		= _pit;
			audio.Play();
		}
	}
}
