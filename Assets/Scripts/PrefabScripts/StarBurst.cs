using UnityEngine;
using System.Collections;

public class StarBurst : MonoBehaviour {

	public GameObject _burst;
	public GameObject _starBkg;
	public GameObject _star;

	bool enable = false;

	void Start()
	{
		SetEnable (enable);
	}

	// Update is called once per frame
	void Update () 
	{
		if(_burst != null)
			_burst.transform.Rotate(0, 0, 100 * Time.deltaTime);
	}

	void SetEnable(bool enable)
	{
		_star.SetActive (enable);
	}
}
