﻿using UnityEngine;
using System.Collections;

public class FantasyCard : MonoBehaviour {

	public Sprite[] ClubCards;
	public Sprite[] DiamondCards;
	public Sprite[] HeartsCard;
	public Sprite[] SpadeCards;

	public SpriteRenderer suitSprite;


	public void UpdateCardDisplay(int cNum, Enums.SUIT suit)
	{
		Sprite[] arrToUse = null;
		
		if(suit == Enums.SUIT.CLUB)
			arrToUse = ClubCards;
		else if(suit == Enums.SUIT.DIAMOND)
			arrToUse = DiamondCards;
		else if(suit == Enums.SUIT.HEART)
			arrToUse = HeartsCard;
		else if(suit == Enums.SUIT.SPADE)
			arrToUse = SpadeCards;
		
		int cardIndex = (cNum - 1);
		
		if(suitSprite && arrToUse != null && (cardIndex <= arrToUse.Length))
		{
			suitSprite.sprite 	= arrToUse [cardIndex];
		}else
		{
			Debug.LogError("Cound not assign the card ");
		}
	}
}
