using UnityEngine;
using System.Collections;

public class LootObject : BaseBoardObject {

	public GameObject[] lootObjs;

	public Enums.LootType CurrType;

	public void SetType(Enums.LootType _type)
	{
		gameObject.SetActive (true);

		foreach (GameObject obj in lootObjs)
			obj.SetActive(false);

		//Debug.Log ("*** Setting Loot Type *** " + _type);

		CurrType = _type;

		GameObject lootObj = lootObjs[(int)CurrType];

		lootObj.SetActive(true);
	}

	public void TossFromPos(Vector3 _pos)
	{
		SetPosition (_pos);
		TossFromPlace ();
	}
}
