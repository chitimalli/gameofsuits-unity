﻿using UnityEngine;
using System.Collections;

public class RectCard : MonoBehaviour {

	public Sprite[] NRectSuits;
	public Sprite[] ARectSuits;
	public Sprite[] JRectSuits;
	public Sprite[] QRectSuits;
	public Sprite[] KRectSuits;

	public SpriteRenderer suitSprite;

	public TextMesh blackTxt;
	public TextMesh redTxt;

	public TextMesh blackTxt2;
	public TextMesh redTxt2;

	public bool showBottomNum = false;

	public void UpdateCardDisplay(CardData _cardData)
	{
		int num 			= _cardData.Num;
		Enums.SUIT _suit	= _cardData.Suit;


		if(blackTxt != null ) blackTxt.text = "";
		if(blackTxt2 != null ) blackTxt2.text = "";
		if(redTxt != null ) redTxt.text = "";
		if(redTxt2 != null ) redTxt2.text = "";

		if(_cardData.Num > 1 && _cardData.Num <= 10)
		{
			suitSprite.sprite = NRectSuits [(int)_suit];

			if(blackTxt != null ) blackTxt.text 	= (_suit == Enums.SUIT.CLUB || _suit == Enums.SUIT.SPADE) ? ""+ CardData.SNum(num) : "";
			if(redTxt != null )redTxt.text 			= (_suit == Enums.SUIT.DIAMOND || _suit == Enums.SUIT.HEART) ? ""+ CardData.SNum(num) : "";

			if(showBottomNum)
			{
				if(blackTxt2 != null ) blackTxt2.text 	= (_suit == Enums.SUIT.CLUB || _suit == Enums.SUIT.SPADE) ? ""+ CardData.SNum(num) : "";
				if(redTxt2 != null )redTxt2.text 		= (_suit == Enums.SUIT.DIAMOND || _suit == Enums.SUIT.HEART) ? ""+ CardData.SNum(num) : "";
			}
		}
		else
		{
			if(_cardData.Num == 1)
			{
				suitSprite.sprite = ARectSuits [(int)_suit];
			}
			else if(_cardData.Num == 11)
			{
				suitSprite.sprite = JRectSuits [(int)_suit];
			}
			else if(_cardData.Num == 12)
			{
				suitSprite.sprite = QRectSuits [(int)_suit];
			}
			else if(_cardData.Num == 13)
			{
				suitSprite.sprite = KRectSuits [(int)_suit];
			}
		}
	}
}
