using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class HandInfoPF : MonoBehaviour {

	public Enums.GAMETYPE HandType;

	public CoinPF 	coinVal;
	public TextMesh handNameTxt;
	public CardObject[] cards;

	int[] zRot = {30,15,0,345,330};

	public void UpdateWithCardObjs(List<BaseBoardObject> selList)
	{

		Debug.Log (">> selList Len"+selList.Count);

		for (int i =0; i< selList.Count; ++i)
		{
			CardObject cCard = cards[i];

			bool show = (i <= selList.Count);
			cCard.gameObject.SetActive(show);

			CardObject selCard = (CardObject)selList[i];

			if(show && selCard != null)
			{
				cCard.Rotation = new Vector3(0,0,zRot[i]);
				cCard.transform.position 	=  cCard.transform.position + new Vector3(0,0,-0.1f * (i+1)); 
				cCard.SetCardData(selCard.Card);
			}
		}
	}
}

