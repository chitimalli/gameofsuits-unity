using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardObject : BaseBoardObject {
	
	// Card Rect Type
	public RectCard 	rectCard;
	public FantasyCard  fanCard;
	public FantasyCard previewCard;

	public SpriteRenderer selectFrame;

	public override Enums.SUIT Suit 	{get{return baseCard.Suit;}}

	void Start()
	{
		UpdateSelector ();
		ResetPreview ();
	}

	public bool SetCardInfo(int cNum, Enums.SUIT cSuit)
	{
		//if(Card == null)
		{
			Card = ScriptableObject.CreateInstance("CardData") as CardData;
			Card.Init(cSuit,cNum);
		}

		return SetCardData (Card);
	}

	public bool SetCardData(CardData card)
	{
		Card = card;

		if(Card == null)
			return false;

		if(rectCard != null)
			rectCard.UpdateCardDisplay (Card);

		if(fanCard != null)
			fanCard.UpdateCardDisplay (Card.Num,Card.Suit);

		return true;
	}

	public override void Select()
	{
		IsSelected = true;
		UpdateSelector ();
		transform.localScale -= new Vector3(0.05F, 0.05F, 0);
		PlaySound (Enums.ACTION_SND.SELECT);
	}
	
	public override void DeSelect()
	{
		IsSelected = false;
		transform.localScale += new Vector3(0.05F, 0.05F, 0);
		if(!IsDirty)
		{
			UpdateSelector ();
		}
	}

	public override void UpdateSelector ()
	{
		base.UpdateSelector ();

		if(selectFrame != null)
		{
			selectFrame.gameObject.SetActive(IsSelected);
//			if(IsSelected)
//			{
//				int randIndex = Random.Range(0,SelectFrames.Length);
//				selectFrame.sprite	= SelectFrames[randIndex];
//			}
		}
	}

	public override void UpdatePreview(int num, Enums.SUIT suit)
	{
		if(previewCard != null)
		{
			previewCard.gameObject.SetActive(true);
			previewCard.UpdateCardDisplay(num,suit);
		}
	}

	public override void ResetPreview()
	{
		if(previewCard != null)
			previewCard.gameObject.SetActive (false);
	}


	public override void PlaySound (Enums.ACTION_SND action, int id = 0)
	{
		switch(action)
		{
			case Enums.ACTION_SND.SELECT:
			case Enums.ACTION_SND.DESELECT:
			{
				//HANDLE MAX:6
				id = (GameBoard.Instance.SelectedObjects.Count) % 7;
				SoundMgr.Instance.PlayCardSound(Enums.CARD_SND.SLIDE,id);
			//SoundMgr.Instance.PlaySelectSound (GameBoard.Instance.SelectedObjects.Count);
				break;
			}
			case Enums.ACTION_SND.DROP:
			{
				SoundMgr.Instance.PlayCardSound(Enums.CARD_SND.SLIDE,id);
				break;
			}
			case Enums.ACTION_SND.WORP:
			case Enums.ACTION_SND.SWAP:
			{
				SoundMgr.Instance.PlayCardSound(Enums.CARD_SND.PLACE,id);
				break;
			}
			case Enums.ACTION_SND.ZAP:
			{
				SoundMgr.Instance.PlayCardSound(Enums.CARD_SND.SHUFFLE,id);
				break;
			}
		}
	}
}
