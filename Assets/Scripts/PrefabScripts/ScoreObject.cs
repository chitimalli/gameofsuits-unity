using UnityEngine;
using System.Collections;

public class ScoreObject : AnimBehaviour {

	public TextMesh frontTxt;
	public TextMesh backTxt;

	int zOrder = -5;
	string str;

	public void SetTxt (string _txt)
	{
		str 			= _txt;
		frontTxt.text 	= str;
		backTxt.text 	= str;
	}

	public void ShowStrAtObjLoc(GameObject _obj,string _str, float _time, float _yOffset)
	{
		str 			= _str;
		frontTxt.text 	= str;
		backTxt.text 	= str;


		SlideToPosition (gameObject, new Vector3(_obj.transform.localPosition.x,_obj.transform.localPosition.y,zOrder), _time);

		StartCoroutine (C.DelayAction (_time,() => {
			Destroy(gameObject);
				}));

	}

	void OnDestroy()
	{
		frontTxt.text 	= "";
		backTxt.text 	= "";
	}
}
