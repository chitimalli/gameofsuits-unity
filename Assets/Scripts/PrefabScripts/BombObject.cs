using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BombObject : BaseBoardObject {
	
	public GameObject Coin3D;
	public GameObject Coin2D;

	public override void Select()
	{

	}
	
	public override void DeSelect()
	{
		if(IsMoving)
			return;

		IsSelected = false;
	}

	public override Color GetLineColor()
	{
		return Color.yellow;
	}
}
