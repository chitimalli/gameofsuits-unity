using UnityEngine;
using System.Collections;

public class PFLoader : MonoBehaviour {

	private static PFLoader m_Instance 	= null;

	public static PFLoader Instance
	{
		get
		{
			return m_Instance;
		}
	}
	
	void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
	}

	public GameObject[] m_RectPrefabs;
	public GameObject   m_LootPrefab;
	public GameObject   m_ProgressItemPrefab;


	public GameObject[] CFXCards;
	public GameObject[] CFXHandText;
	public GameObject[] CFXDebris;

	public GameObject[] CFXEffects;


	public CardObject 		CardPrefabObj		{get{return (Instantiate(PrefabArray[0]) as GameObject).GetComponent<CardObject>();}}
	public SuitObject 		SuitsPrefabObj		{get{return (Instantiate(PrefabArray[1]) as GameObject).GetComponent<SuitObject>();}}
	public CoinObject 		PickupPrefabObj		{get{return (Instantiate(PrefabArray[2]) as GameObject).GetComponent<CoinObject>();}}
	public ScoreObject  	ScorePrefabObj		{get{return (Instantiate(PrefabArray[3]) as GameObject).GetComponent<ScoreObject>();}}
	public BaseBoardObject  EmptyPrefabObj		{get{return (Instantiate(PrefabArray[4]) as GameObject).GetComponent<BaseBoardObject>();}}

	public LootObject   LootPrefabObj			{get{return (Instantiate(m_LootPrefab) as GameObject).GetComponent<LootObject>();}}

	// Text Effects
	public GameObject ZAPPrefabTxt	{get{return Instantiate(CFXHandText[0]) as GameObject;}}


	GameObject[]	PrefabArray	
	{
		get
		{ 
			return m_RectPrefabs;

		}
	}

	public void ShowEffect(Vector3 effPos,Enums.CFX cfx)
	{
		GameObject effect = Instantiate(CFXEffects[(int)cfx]) as GameObject;
		
//		int xOff = 0;
//		int yOff = 0;
		
		if(effect != null)
		{
			effect.SetActive(true);
			for(int i = 0; i < effect.transform.childCount; i++)
				effect.transform.GetChild(i).gameObject.SetActive(true);
			
//			if(cfx == Enums.CFX.THUNDER)
//			{
//				xOff = -3;
//				yOff = -1;
//			}
//			else if(cfx == Enums.CFX.SLASH)
//			{
//				xOff = 0;
//				yOff = -1;
//			}

			effect.transform.position = effect.transform.position;
		}
	}

	public void ShowEffect(int _row,int _col, Enums.CFX cfx)
	{
		GameObject effect = Instantiate(CFXEffects[(int)cfx]) as GameObject;

		int xOff = 0;
		int yOff = 0;

		if(effect != null)
		{
			effect.SetActive(true);
			for(int i = 0; i < effect.transform.childCount; i++)
				effect.transform.GetChild(i).gameObject.SetActive(true);

			if(cfx == Enums.CFX.THUNDER)
			{
				xOff = -3;
				yOff = -1;
			}
			else if(cfx == Enums.CFX.SLASH)
			{
				xOff = 0;
				yOff = -1;
			}

			effect.transform.position = GameBoard.RowColPos(_row + yOff ,_col + xOff,-10) + effect.transform.position;
		}
	}

	public void ShowZapEffect(int _row,int _col)
	{
		GameObject effect = ZAPPrefabTxt;

		if(effect != null)
		{

			effect.SetActive(true);
			for(int i = 0; i < effect.transform.childCount; i++)
				effect.transform.GetChild(i).gameObject.SetActive(true);

			effect.transform.position = GameBoard.RowColPos(_row -1 ,_col -3,-10) + effect.transform.position;
		}
	}

	public void ShowDebryAt(int _row,int _col)
	{
		int index = 0;
		GameObject effect = null;
		if(index < CFXCards.Length)
		{
			effect = (GameObject)Instantiate (CFXDebris [index]);
		}
		
		if(effect != null)
		{

			effect.SetActive(true);
			for(int i = 0; i < effect.transform.childCount; i++)
				effect.transform.GetChild(i).gameObject.SetActive(true);

			effect.transform.position = GameBoard.RowColPos(_row,_col,-10) + effect.transform.position;
		}
	}
	
	public void ShowSuitEffectAt(int _row,int _col,Enums.SUIT _suit)
	{
		int index = (int)_suit;
		GameObject effect = null;
		if(index < CFXCards.Length)
		{
			effect = (GameObject)Instantiate (CFXCards [index]);
		}
		
		if(effect != null)
		{

			effect.SetActive(true);
			for(int i = 0; i < effect.transform.childCount; i++)
				effect.transform.GetChild(i).gameObject.SetActive(true);

			effect.transform.position = GameBoard.RowColPos(_row,_col) + effect.transform.position;
		}
	}

	
//	public void ShowScoreObjAt(GameObject _obj, string _str,float _time = 0.5f, float _yOffset = 0.75f)
//	{
//		ScoreObject scoreObj 				= PFLoader.Instance.ScorePrefabObj;
//		
//		if(scoreObj != null && scoreObj.CompareTag(Config.SCOREOBJ))
//		{
//			scoreObj.ShowStrAtObjLoc(_obj,_str,_time,_yOffset);
//			scoreObj.transform.parent   = transform;
//		}
//	}
}
