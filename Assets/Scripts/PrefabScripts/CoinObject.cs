using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoinObject : BaseBoardObject {

	public override void Select()
	{
		IsSelected = true;
		UpdateSelector ();

		transform.localScale -= new Vector3(0.15F, 0.15F, 0);
		RotateOnce (0.5f);
		PlaySound (Enums.ACTION_SND.SELECT);
	}
	
	public override void DeSelect()
	{
		IsSelected = false;
		UpdateSelector ();
		transform.localScale += new Vector3(0.15F, 0.15F, 0);
	}

	public override void UpdateSelector()
	{
		if(m_selector != null)
		{
			m_selector.SetActive (IsSelected);
		}
	}

	public override void PlaySound (Enums.ACTION_SND action, int id = 0)
	{
		switch(action)
		{
			case Enums.ACTION_SND.SELECT:
			case Enums.ACTION_SND.DESELECT:
			{
				SoundMgr.Instance.PlayLootSound(Enums.LootType.COIN_LOOT,0.15f,Random.Range(1.0f,2.0f));
				//SoundMgr.Instance.PlayLootSound(Enums.LootType.COIN_LOOT,id);
				break;
			}
			case Enums.ACTION_SND.DROP:
			{
				SoundMgr.Instance.PlayLootSound(Enums.LootType.COIN_LOOT,0.15f,Random.Range(1.0f,2.0f));
			break;
			}
			case Enums.ACTION_SND.WORP:
			case Enums.ACTION_SND.SWAP:
			{
				SoundMgr.Instance.PlayLootSound(Enums.LootType.COIN_LOOT,id);
			break;
			}
			case Enums.ACTION_SND.ZAP:
			{

				break;
			}
		}
	}
		
	public override Color GetLineColor()
	{
		return Color.yellow;
	}
}
