﻿using UnityEngine;
using System.Collections;

public class HeartObject : MonoBehaviour
{
	public GameObject heart;
	public GameObject heartBkg;

	bool m_isfull = false;

	public void Start()
	{
		//IsFull = false;
	}

	public bool IsFull
	{
		get 
		{
			return m_isfull;
		} 

		set
		{
			m_isfull = value;
			heart.SetActive(m_isfull);
		}
	}
}