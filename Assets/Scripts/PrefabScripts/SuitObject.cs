using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SuitObject : BaseBoardObject {

	public GameObject[] ChipSuits;
	public GameObject  	suitChip;
	public FantasyCard previewCard;

	public override Enums.SUIT Suit
	{
		get{return baseSuit;}

		set{

			baseSuit = value;
			for (int i = 0; i< ChipSuits.Length; ++i)
			{
				bool enable = ((int)baseSuit == i);
				(ChipSuits[i]).SetActive(enable);
			}
		}
	}

	void Start()
	{
		ResetPreview ();
	}

	public override void Select()
	{
		IsSelected = true;
		UpdateSelector ();

		transform.localScale -= new Vector3(0.15F, 0.15F, 0);
		RotateOnce (0.5f);
		PlaySound (Enums.ACTION_SND.SELECT);
	}

	public override void DeSelect()
	{
		IsSelected = false;
		UpdateSelector ();

		transform.localScale += new Vector3(0.15F, 0.15F, 0);
	}

	public override void UpdatePreview(int num, Enums.SUIT suit)
	{
		if(previewCard != null)
		{
			previewCard.gameObject.SetActive(true);
			previewCard.UpdateCardDisplay(num,suit);
		}
	}

	public override void ResetPreview()
	{
		if(previewCard != null)
			previewCard.gameObject.SetActive (false);
	}

	public override Color GetLineColor()
	{
		switch(Suit)
		{
			case Enums.SUIT.SPADE:{return Color.gray;}
			case Enums.SUIT.CLUB:{return Color.grey;}
			case Enums.SUIT.DIAMOND:{return Color.red;}
			case Enums.SUIT.HEART:{return Color.red;}
		}
		return Color.yellow;
	}


	public override void PlaySound (Enums.ACTION_SND action, int id = 0)
	{
		switch(action)
		{
			case Enums.ACTION_SND.SELECT:
			case Enums.ACTION_SND.DESELECT:
			{
				//HANDLE MAX:6
				id = (GameBoard.Instance.SelectedObjects.Count) % 5;
				SoundMgr.Instance.PlayChipSound(Enums.CHIP_SND.HANDLE,id);
				break;
			}
			case Enums.ACTION_SND.DROP:
			{
				id = (RowID + ColID) % 4;
				SoundMgr.Instance.PlayChipSound(Enums.CHIP_SND.COLLIDE,id);

				break;
			}
			case Enums.ACTION_SND.WORP:
			case Enums.ACTION_SND.SWAP:
			{
				//LAY MAX:6
				id = (GameBoard.Instance.SelectedObjects.Count) % 2;
				SoundMgr.Instance.PlayChipSound(Enums.CHIP_SND.LAY,id);

				break;
			}
			case Enums.ACTION_SND.ZAP:
			{
				SoundMgr.Instance.PlayChipSound(Enums.CHIP_SND.STACK,id);
				break;
			}
		}
	}
}
