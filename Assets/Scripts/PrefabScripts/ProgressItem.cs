using UnityEngine;
using System.Collections;

public class ProgressItem : FFUIButton 
{
	/// <summary>
	/// Settings for each Level
	/// </summary>

	public int GridRows 		= 4;
	public int GridColumns 		= 4;
	public int MaxSpawnCards 	= 7;

	//Deck variants
	public bool ReuseBurntCard			= true;
	public bool ReuseZappedCard			= true;
	
	public Enums.DIFFICULTY	diff;
	public bool SwapEnabled						= true;
	public bool SwapOnBoardCard 				= true;

	public Enums.GAMEMODE GameMode;

	// Upgradable Boost
	public int FreeSwaps 	= 25;
	public int FreeZaps  	= 15;
	public int FreeZips 	= 10;

	// Onetime use Boost
	public int FreeFreezes 	= 0;
	public int FreeShuffles = 0;

	public float StartTime		= C.GAME_TIME;
	public int startMoves		= 10;
	public float BuffTimeLeft 	= 0;

	public void UpdateGameSetings()
	{
		if(diff == Enums.DIFFICULTY.EASY)
		{
			GridRows 			= 2;
			GridColumns 		= 3;
			MaxSpawnCards 		= 3;
		
			FreeSwaps 	= 10;
			FreeZaps  	= 5;
			FreeZips 	= 10;

			BuffTimeLeft 	= 0;
		}
		else if(diff == Enums.DIFFICULTY.MEDIUM)
		{
			GridRows 			= 5;
			GridColumns 		= 4;
			MaxSpawnCards 		= 7;
			
			FreeSwaps 	= 15;
			FreeZaps  	= 10;
			FreeZips 	= 15;

			BuffTimeLeft 	= 0;
		}
		else if(diff == Enums.DIFFICULTY.HARD)
		{
			GridRows 			= 5;
			GridColumns 		= 4;
			MaxSpawnCards 		= 7;
			
			FreeSwaps 	= 25;
			FreeZaps  	= 20;
			FreeZips 	= 25;

			BuffTimeLeft 	= 0;
		}
	}
}
