﻿using UnityEngine;
using System.Collections;

public class PreviewObj : MonoBehaviour {

	public TextMesh 		previewTxtB;
	public TextMesh 		previewTxtR;

	public SpriteRenderer	previewSuit;
	public SpriteRenderer	previewBkg;

	public Sprite[] 		suits;

	public void UpdatePreview(int num, Enums.SUIT suit)
	{
		previewSuit.sprite 	= suits [(int)suit];

		if(suit == Enums.SUIT.DIAMOND || suit == Enums.SUIT.HEART)
		{
			previewTxtB.text		= "";
			previewTxtR.text		= CardData.SNum(num);
		}else
		{
			previewTxtB.text		= CardData.SNum(num);
			previewTxtR.text		= "";
		}
	}
}
