using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseBoardObject : MonoBehaviour {

	bool m_isSelected 	= false;
	bool m_isDirty 		= false;
	bool m_isMoving 	= false;

	bool m_zapMode		= false;
	bool m_swapMode		= false;

	int row 			= 0;
	int col 			= 0;
	int zOrder			= 0;
	Vector2	posDelta	= Vector2.zero;

	int handScore		= 0;
	int bonusTime		= 0;
	int m_clickCount 	= 0;
	int m_bonusMultiplier	= 1;

	Vector3 originalPos;


	List<BaseBoardObject>  m_neighbours = new List<BaseBoardObject>() ;

	protected 	Enums.SUIT baseSuit;
	protected CardData	baseCard;
 
	public GameObject 		m_selector = null;

	public bool IsSelected	{get{return m_isSelected;} set{m_isSelected = value;}}
	public bool IsDirty		{get{return m_isDirty;} set{m_isDirty = value;}}
	public bool IsMoving	{get{return m_isMoving;} set{m_isMoving = value;}}

	public int RowID		{get{return row;} set{row = value;}}
	public int ColID		{get{return col;} set{col = value;}}
	public int ZOrder		{get{return zOrder;} set{zOrder = value;}}
	public Vector2 PosDelta	{get{return posDelta;} set{posDelta = value;}}
	public Vector3 OrigPos	{get{return originalPos;} set{originalPos = value;}}

	public bool IsInZapMode		{get{return m_zapMode;} set{m_zapMode = value;}}
	public bool IsInSwapMode	{get{return m_swapMode;} set{m_swapMode = value;}}

	public int 	HandScore		{get{return handScore;} set{handScore = value;}}
	public int 	BonusTime		{get{return bonusTime;} set{bonusTime = value;}}
	public int 	ClickCount		{get{return m_clickCount;} set{m_clickCount = value;}}
	public int 	Bonus			{get{return m_bonusMultiplier;} set{m_bonusMultiplier = value;}}

	public virtual Enums.SUIT Suit 	{get{return baseSuit;} set{baseSuit = value;}}
	public virtual CardData Card	{get{return baseCard;} set{baseCard = value;}}

	public BaseBoardObject  baseObj;
	//public Enums.OJB_TYPE 	objType;

	public List<BaseBoardObject> 	Neighbours {get{return m_neighbours;} set{m_neighbours = value;}}


	//Fro Animation
	public Vector3 Velocity;
	public Vector3 Acceleration;
	public Vector3 Rotation;
	public float Speed;
	
	public float? RenderDelay;
	private float _renderTime;
	
	public virtual void Update()
	{
		transform.Translate(Velocity * Time.deltaTime * Speed, Space.World);
		transform.Rotate(Rotation * Time.deltaTime * Speed);
		Velocity += Acceleration * Time.deltaTime * Speed;
		
		if (RenderDelay.HasValue && Time.time>=_renderTime)
		{
			RenderDelay = null;
		}
	}
	
	public void SetRenderDelay(float renderDelay)
	{
		if (renderDelay > 0f)
		{
			RenderDelay = renderDelay;
			_renderTime = Time.time + renderDelay;
		}
	}

	public void ResetAnimation()
	{
		Speed 		= 0;
		RenderDelay = null;
	}

	protected virtual void StopShake()
	{
		iTween.Stop(gameObject);
		ResetToOriginal ();
	}
	
	public virtual void RotateOnce(float resetTime = 0.5f)
	{
		if(baseObj != null)
		{
			baseObj.Speed 			= 1.0f;
			baseObj.Velocity 		= C.Range(Vector3.zero, Vector3.zero);
			baseObj.Acceleration 	= Vector3.zero;
			baseObj.Rotation 		= C.Range(Vector3.down * 360, Vector3.down * 360);
			baseObj.SetRenderDelay(0.1f);
			
			StartCoroutine (C.DelayAction(resetTime,()=> {
				baseObj.gameObject.transform.rotation = Quaternion.identity; 
				baseObj.ResetAnimation ();
			}));
		}
	}
	
	public virtual void SetBonus(int bonus){ Bonus = bonus;}

	public virtual void PlaySound (Enums.ACTION_SND action, int id = 0){}


	void Start()
	{
		if(m_selector != null)
		{
			m_selector.SetActive(false);
		}
	}

	public virtual void UpdatePreview(int num, Enums.SUIT suit){}
	public virtual void ResetPreview(){}

	public virtual void Select()
	{
		IsSelected = true;
	}
	
	public virtual void DeSelect()
	{
		IsSelected = false;
	}

	public virtual void UpdateSelector()
	{
		if(m_selector != null)
		{
			m_selector.SetActive (IsSelected);
		}
	}

	public virtual bool SameSuit(BaseBoardObject _obj)
	{
		return _obj.Suit == Suit;
	}

	public virtual Color GetLineColor()
	{
		if(Suit == Enums.SUIT.SPADE || Suit == Enums.SUIT.CLUB)
			return Color.black;
		else 
			return Color.red;
	}

	public virtual void UpdateNeighbours(List<BaseBoardObject> allBoardItems)
	{
		Neighbours.Clear ();

		foreach(BaseBoardObject obj in allBoardItems)
		{
			int dX 		=  Mathf.Abs(obj.RowID - RowID);
			int dY 		=  Mathf.Abs(obj.ColID - ColID);
			int sumdX	= dX + dY;

			if(sumdX == 1)
			{
				//Enums.DIRECTION dir =  Enums.Vec2NEWS(new Vector2(dX,dY));
				//int arrID = (int)dir;
				Neighbours.Add(obj);
			}
		}

		//Debug.Log ("**** Neighbours count " + Neighbours.Count);
	}
	
	public void SetRowColZID(int row,int col, int z)
	{
		RowID = row;
		ColID = col;
		ZOrder = z;
	}

	public virtual bool IsNeighbour(BaseBoardObject _obj,bool allowDiag)
	{
		bool isNeighbour = false;

		int dX 		=  Mathf.Abs(_obj.RowID - RowID);
		int dY 		=  Mathf.Abs(_obj.ColID - ColID);
		int sumdX	= dX + dY; 
		
		//Debug.Log("** dX "+dX+" dY "+dY+" Sum: "+sumdX + " AllowDiagCards: "+ allowDiag);
		
		if(allowDiag && dX == 1 && dY == 1)
			isNeighbour = true;
		else if(sumdX == 1)
			isNeighbour = true;
		else
			isNeighbour = false;

		return isNeighbour;
	}
	
	public void FallDown(float t = 0.15f)
	{
		if(PosDelta != Vector2.zero)
		{
			RowID += (int)PosDelta.y;
			ColID += (int)PosDelta.x;
			JumpToPosition(GameBoard.RowColPos(RowID,ColID,ZOrder),t);
		}
	}

	#region Move to from Anim

	public void SetPosition(Vector3 _pos)
	{
		OrigPos 			= _pos;
		transform.position 	= OrigPos;
		PosDelta = Vector2.zero;
	}

	public void JumpToPosition(Vector3 _pos, float _time = 0.15f, EaseType _easeType = EaseType.easeOutBounce)
	{
		OrigPos 		= _pos;
		gameObject.MoveTo (OrigPos, true, _time, 0.0f, _easeType);
		PosDelta = Vector2.zero;
	}

	public void SpawnInPlace()
	{
		gameObject.transform.localScale = Vector3.zero;
		iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "islocal", true, "time", 0.5f,"easetype","spring"));
	} 

	protected void ResetToOriginal()
	{
		transform.localPosition = OrigPos;
		this.transform.rotation = Quaternion.identity;
	}
	
	void DeActivate()
	{
		DeSelect ();
		IsDirty = true;
		gameObject.SetActive (false);
	}

	#endregion



	public void TrackClickCount()
	{
		if(!CompareTag(C.EMPTYOBJ))
		{
			ClickCount += 1;
			
			StartCoroutine(C.DelayAction(C.DBLCLICK_TIME,() => {
				ClickCount = 0;
			}));
		}
	}

	public bool ZapIt()
	{
		if(ClickCount >= C.DBLCLICK_COUNT)
		{
			DeActivate ();
			return true;
		}

		return false;
	}

	public void TossFromPlace()
	{
		int vDir = 1;

		Vector3 MinVelocity = new Vector3 (0,5 * vDir,0);
		Vector3 MaxVelocity = new Vector3 (0,5 * vDir,0);
		
		Vector3 MinRotation = new Vector3 (360,360,360);
		Vector3 MaxRotation = new Vector3 (360,360,360);
		
		Vector3 accel 		= new Vector3 (0,-10 * vDir,0);
		
		float LifeTime 		= 5.5f;
		float StartSpeed 	= 1.0f;
		float RenderDelay 	= 0.1f;
		
		//MovingObject movingObject = spawnedObject.gameObject.AddComponent<MovingObject>();
		Velocity 		= C.Range(MinVelocity, MaxVelocity);
		Speed 			= StartSpeed;
		Acceleration 	= accel;
		Rotation 		= C.Range(MinRotation, MaxRotation);
		SetRenderDelay(RenderDelay);
		Destroy(gameObject, LifeTime);
		
		IsMoving = true;
	}

	public void MarkClean()
	{
		gameObject.SetActive (true);
		IsDirty 		= false;
		PosDelta 		= Vector2.zero;
		ClickCount 		= 0;
	}

	public void MarkDirty()
	{
		if(IsDirty)
			return;

		DeActivate ();
		StopAllCoroutines ();
		//HOTween.Complete ();
	}

	void OnDestroy()
	{
		MarkDirty ();
	}

//	protected virtual void OnShow()
//	{
//		//SoundMgr.Instance.PlayUISound (Enums.UISoundID.UI_Woosh);
//	}
//	
//	protected virtual void OnHide(){}
//
//	public virtual void ShowObject(float _time = 0.15f)
//	{
//		Anim.SpringContainer (gameObject, hidePos, showPos,hideScale,showScale, 0.75f);
//		IsShowing = true;
//		OnShow ();
//	}
//	
//	public virtual void HideObject(float _time = 0.0f)
//	{
//		Anim.SpringContainer (gameObject, showPos, hidePos,showScale,hideScale, _time);
//		IsShowing = false;
//		
//		OnHide ();
//	}


	#region boost
	public void ToggleZapMode(bool _enable)
	{
		IsInZapMode = _enable;
		
		if(!IsInZapMode)
			transform.rotation = Quaternion.identity;
		else
			transform.rotation = Quaternion.Euler(0,0,30);
	}

	public void ToggleSwapMode(bool _enable)
	{
		IsInSwapMode = _enable;
		
		if(!IsInSwapMode)
			transform.rotation = Quaternion.identity;
		else
			transform.rotation = Quaternion.Euler(0,0,-30);
	}
	#endregion
}
