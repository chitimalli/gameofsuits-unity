﻿using UnityEngine;
using System.Collections;

public class AnimBehaviour :MonoBehaviour
{
	public void SlideToPosition(GameObject _obj, Vector3 toPos, float _time,System.Action callback = null)
	{
		iTween.MoveTo(_obj, iTween.Hash("position", toPos, "islocal", true, "time", _time,"easetype","spring"));
	}

	public void TeleportToPosition(GameObject _obj, Vector3 toPos, float _time,System.Action callback = null)
	{
		iTween.MoveTo(_obj, iTween.Hash("position", toPos, "islocal", true, "time", _time,"easetype","spring"));
	}
}
