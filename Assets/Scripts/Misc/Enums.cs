using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Enums : MonoBehaviour {

	public enum DIFFICULTY
	{
		EASY 	= 0,
		MEDIUM	= 1,
		HARD	= 2,
		INSANE	= 3
	}

	public enum BOOST
	{
		SWAP = 0,
		ZAP,
		ZIP,
		BOMB,
		SHUFFLE,
		COIN,
		LIFE,
	}

	public enum CFX
	{
		SLASH,
		THUNDER,
	}

	public enum LootType
	{
		COIN_LOOT = 0,
		FREEZE_LOOT,
		HANDS_LOOT,
		SWAP_LOOT,
		ZAP_LOOT,
		MAX_LOOT
	}


	public enum SUIT
	{
		SPADE 	= 0,
		CLUB	= 1,
		DIAMOND	= 2,
		HEART	= 3
	}

	public enum GAMEMODE
	{
		MOVES		= 0,
		TIMED		,
		UNLIMITED	,
		CHALLENGE	,
		TUTORIAL
	}

	public enum GAMETYPE
	{
		THREE_CARD		= 0,
		FIVE_CARD,
		BLACK_JACK
	}
	
	public enum DIRECTION
	{
		NORTH	= 0,
		EAST	= 1,
		WEST	= 2,
		SOUTH	= 3,
		NEWS	= 4
	}

	public enum PICKUP
	{
		COIN	= 0,
		BOMB	= 1,
		DSCORE
	}

	public enum POPUP_TYPE
	{
		TITLE,
		STORE,
		BOOST,
		LEVEL,
		PAUSE,
		DROPDOWN,
		HANDINFO,
		FTUE,
		MULLIGAN
	}

	public enum POPUP_TAB
	{
		//Store Tabs
		ZIP,
		ZAP,
		SWAP,
		BOMB,
		SHUFFLE,
		UPGRADE,

		COINS,
		LIVES,

		//Level Tabs
		START,
		RESULTS,

		//PauseTab
		PAUSE,
		MULLIGAN,

		//DropDown Tabs
		SETTINGS,
		MENU,

		//HandInfo Tabs
		FIVE_HAND,
		THREE_HAND,

		//FTUE Tabs
		FTUE_DRAG,
		FTUE_ZIP,
		FTUE_ZAP,
		FTUE_SWAP,
		FTUE_MATCH
	}

	public enum HTYPE
	{
		HIGH_CARD 		= 0,	 	
		ONE_PAIR		,		// MINI & FULL	
		TWO_PAIR		,		// FULL
		THREE_OFA_KIND	,		// MINI & FULL
		STRAIGHT		,		// MINI & FULL
		FLUSH			,		// MINI & FULL
		FULL_HOUSE		,		// FULL
		FOUR_OFA_KIND	,		// FULL
		STRAIGHT_FLUSH	,		// MINI & FULL
		ROYAL_FLUSH		,		// MINI & FULL
		INVALID
	}

	public enum MHTYPE
	{
		M_PAIR 		= 0,
		M_FLUSH 	,
		M_STRAIGHT 	,
		M_THREE_OFA_KIND,
		M_STRAIGHT_FLUSH,
		INVALID
	}

	public enum FEEDBACK_SND
	{
		GOOD 		= 0,
		GREAT		= 1,
		AMAZING		= 2,
		EXCELLENT, 
		NO,
		LETSGETSTARTED,
		HALFTIME,
		TIMESUP
	}

	public enum CARD_SND
	{
		PLACE,
		SHOVE,
		SLIDE,
		SHUFFLE
	}

	public enum CHIP_SND
	{
		LAY,
		COLLIDE,
		HANDLE,
		STACK
	}


	public enum UISoundID
	{
		UI_Click 	= 0,
		UI_Congrats = 1,
		UI_Down 	= 2,
		UI_Woosh 	= 3,
	}

	public enum ACTION_SND
	{
		SELECT,
		DESELECT,
		SWAP,
		WORP,
		DROP,
		ZAP
	}

	public static Vector2 DirToVec2(DIRECTION dir)
	{
		Vector2 delVec = Vector2.zero;
		
		switch(dir)
		{
		case Enums.DIRECTION.EAST:
		{
			delVec =  new Vector2(1,0);
			break;
		}
		case Enums.DIRECTION.WEST:
		{
			delVec = new Vector2(-1,0);
			break;
		}
		case Enums.DIRECTION.NORTH:
		{
			delVec = new Vector2(0,1);
			break;
		}
		case Enums.DIRECTION.SOUTH:
		{
			delVec = new Vector2(0,-1);
			break;
		}
		}
		return delVec;
	}

	public static DIRECTION Vec2NEWS(Vector2 vec2d)
	{
		DIRECTION direct = DIRECTION.NEWS;

		int dX 		=  Mathf.Abs((int)vec2d.x);
		int dY 		=  Mathf.Abs((int)vec2d.y);
		int sumdX	= dX + dY;
		
		if(sumdX == 1)
		{
			//NEWS
			if(dX == 0 && dY == 1)		{ direct =  DIRECTION.NORTH;}	// North	
			else if(dX == 1 && dY == 0)	{ direct =  DIRECTION.EAST;}	// East
			else if(dX == -1 && dY == 0) { direct =  DIRECTION.WEST;}	// West
			else { direct =  DIRECTION.SOUTH;}						// South
		}

		return direct;
	}

	public static SUIT RandSuit()
	{
		int rand = Random.Range (0, 4);
		if(rand == 4) rand = 3;
		
		return (Enums.SUIT)rand;
	}

	public static PICKUP RandPickup()
	{
		int rand = Random.Range (0, 2);
		if(rand == 2) rand = 0;
		
		return (PICKUP)rand;
	}
}
