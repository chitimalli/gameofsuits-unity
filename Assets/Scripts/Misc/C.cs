﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class C  
{
	public static GameBoard GB = GameBoard.Instance;
	public static Game G 		  = Game.Instance;

	public static int EMPTY_Z 		= -2;
	public static int CARD_Z 		= -3;

	// Boost
	public static int START_SWAPS 		= 10;
	public static int START_ZAPS 		= 10;
	public static int START_FREEZES 	= 5;
	public static int START_DECK_PCK 	= 1;
	public static int MIN_FOR_SQUARE	= 4;

	public static float FREEZE_TIME 	= 10.0f;
	public static int   START_COINS 	= 100;

	public static int FULL_HAND_SIZE			= 5;
	public static int MINI_HAND_SIZE  	= 3;
	public static int MIN_COIN_SEL		= 2;
	public static int MIN_SUIT_SEL		= 2;
	public static int MIN_SWAP_SEL		= 2;
	public static int MIN_MIX_SEL		= 2;


	public static int START_LIFE 		= 1;
	public static int MAX_LIFE 			= 5;
	public static int LIFE_FILLRATE 	= (int)(60 * 0.5f);

	// Game Constants
	public static int 	XP_LEVELS 			= 100;
	public static int 	PROGRESS_FORCE_MUL  = 5;

	public static int START_MOVES 			= 40;
	public static int 	GAME_TIME 			= 60;
	public static int 	GAME_RELAY_TIME 	= 45;
	public static float BUFF_TIME 			= 0;


	public static int MIN_ROWS				= 4;
	public static int MAX_ROWS				= 6;
	public static int MIN_COLS				= 4;
	public static int MAX_COLS				= 5;

	public static int ABS_MAX_ROWS			= 6;
	public static int ABS_MAX_COLS			= 5;

	public static int   DBLCLICK_COUNT 	= 2;
	public static float DBLCLICK_TIME 	= .25f;
	public static int MAX_SUITTIME 		= 10;
	public static int LEVEL_TIME_STEP 	= 30;


	public static string SUITOBJ 	= "SuitObj";
	public static string CARDOBJ 	= "CardObj";
	public static string COINOBJ 	= "CoinObj";
	public static string SCOREOBJ 	= "ScoreObj";
	public static string LOOTOBJ 	= "LootObj";
	public static string EMPTYOBJ 	= "EmptyObj";

	public static Vector3 Range(Vector3 min, Vector3 max)
	{
		return new Vector3(
			Random.Range(min.x, max.x),
			Random.Range(min.y, max.y),
			Random.Range(min.z, max.z));
	}

	public static Vector2 Range(Vector2 min, Vector2 max)
	{
		return new Vector2 (
			Random.Range (min.x, max.x),
			Random.Range (min.y, max.y));
	}

	public static Vector2 Delta(Vector2 end, Vector2 start)
	{
		return new Vector2 (end.x - start.x, end.y - start.y);
	}

	public static IEnumerator DelayAction(float dTime, System.Action callback)
	{
		yield return new WaitForSeconds(dTime);
		callback();
	}
}
