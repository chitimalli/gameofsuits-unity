using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This class is going to be serializable, and will be the one that will be responsible for
// Game MODE
// Game persistance
// Game Progress
// Game Boost selections
// Game Score & Target's
// Game Timer
// Saves a game in progress

public class Game : MonoBehaviour
{
	// Static instance variables
	public static Game Instance;
	void Awake()
	{
		DontDestroyOnLoad(this);
		Instance = this;
	}

	// PRIVATE //
	List<DeckData>	m_decks	= null;
	int m_currDeckID;

	List<HandData> 		m_handsPlayed 				= new List<HandData>();


	int  	m_totalNumMoves	;		// This is for reference
	int  	m_suitHands	;			// This is for reference
	int  	m_coinsWonInGame	;			// This is for reference

	float  	m_totalTimeSec 		= 0.0f;		// Time in seconds
	float 	m_timeLeftInSec		= 0.0f;
	float 	m_buffTimeLeft		= 0.0f;
	float 	m_freezeTimeLeft	= 0.0f;
	bool 	m_miniHandsValid	= false;
	bool 	m_fullHandsValid	= false;

	int 	m_totalScore 		= 0;
	int 	m_totalXP 			= 0;
	int 	m_startDecks		= 0;

	bool m_isGameStarted		= false;
	bool m_isGameFinished			= true;
	bool m_isPaused				= false;
	bool m_isFrozen				= false;
	bool m_isBuffed				= false;

	int m_progressLevel			= 0;
	bool m_allowDiagCards		= true;			// This lets you know if the game allows Diagonal based selection
	bool m_allowDiagSuites		= true;			// This lets you know if the game allows Diagonal based selection

	bool m_isBacktrackEnabled 	= false;
	bool m_suitsCreateCards		= false;
	bool m_suitsZapCards		= false;
	bool m_cardValLoopAround 	= false;
	bool m_reuseBurntCard		= false;
	bool m_reuseZappeCard		= false;
	bool m_swapBoardCard		= false;
	bool m_HighCardValid		= false;

	Enums.DIRECTION	m_fillDir	= Enums.DIRECTION.SOUTH;

	float nextFullBonusTime		= 15.0f;
	float currBonusTime			= 0.0f;
	int maxCardsOnBord			= 7;
	int suitSpawnRate			= 0;
	int maxMulligans			= 0;
	int mulligansUsed			= 0;
	int 	m_gameLevel 		= 0;
	int m_currHandID;

	int miniHands				= 0;
	int fullHands				= 0;


	Enums.GAMEMODE m_gameMode 	= Enums.GAMEMODE.TIMED;
	Enums.GAMETYPE m_gamePlay 	= Enums.GAMETYPE.THREE_CARD;
	Enums.DIFFICULTY m_gameDiff	= Enums.DIFFICULTY.EASY;

	public static int GridRows 		= 3;
	public static int GridColumns 	= 4;
	
	public static float boardColOffset 	= 1.0f;
	public static float boardRowOffset 	= 1.0f;
	public static float boardXRes 		= 1.0f;
	public static float boardYRes 		= 1.0f;

	// Accessor functions 
	public int  CurrDeckID			{get{return m_currDeckID;} set{m_currDeckID = value;}}
	public Enums.GAMEMODE GameMode 	{get{return m_gameMode;} set{m_gameMode = value;}}
	public Enums.GAMETYPE GamePlay 	{get{return m_gamePlay;} set{m_gamePlay = value;}}
	public Enums.DIFFICULTY GameDiff{get{return m_gameDiff;} set{m_gameDiff = value;}}
	public int GameLevel 			{get{return m_gameLevel;} set{m_gameLevel = value;}}

	
	public int TotalNumMoves	{get{return m_totalNumMoves;} set{m_totalNumMoves = value;}}
	public float TotalGameTime	{get{return m_totalTimeSec;} set{m_totalTimeSec = value;}}
	public int MovesLeft		{get{return (TotalNumMoves - HandsPlayedCount);}}
	public int TotalScore		{get{return m_totalScore;} set{m_totalScore = value;}}
	public int TotalXP			{get{return m_totalXP;} set{m_totalXP = value;}}

	public List<DeckData> 		Decks				{get{return m_decks;}}
	public List<HandData>		HandsPlayed			{get{return m_handsPlayed;} set{m_handsPlayed = value;}}
	public int					HandsPlayedCount	{get{return m_handsPlayed.Count + m_suitHands;}}//
	public int 					SuitHandsCount		{get{return m_suitHands;} set{m_suitHands = value;}}
	public int 					CoinWonInGame		{get{return m_coinsWonInGame;} set{m_coinsWonInGame = value;}}
	public int 					ProgressLevel		{get{return m_progressLevel;} set{m_progressLevel = value;}}

	public int MiniHandsCount 	{get{return miniHands;} set{miniHands = value;}}
	public int FullHandsCount 	{get{return fullHands;} set{fullHands = value;}}

	public float 				TimeLeft			{get{ return m_timeLeftInSec;} set{m_timeLeftInSec = value;}}
	public float 				BuffTimeLeft		{get{ return m_buffTimeLeft;} set{m_buffTimeLeft = value;}}
	public float 				FreezeTimeLeft		{get{ return m_freezeTimeLeft;} set{m_freezeTimeLeft = value;}}

	public float 				NextFullBonusTime	{get{ return nextFullBonusTime;} set{nextFullBonusTime = value;}}
	public float 				CurrBonusTime		{get{ return currBonusTime;} set{currBonusTime = value;}}

	public bool 				IsStarted			{get{return m_isGameStarted;} set{m_isGameStarted = value;}}
	public bool 				IsFinished			{get{return m_isGameFinished;} set{m_isGameFinished = value;}}
	public bool 				IsPaused			{get{return m_isPaused;} set{m_isPaused = value;}}
	public bool 				IsBuffed			{get{return m_isBuffed;} set{m_isBuffed = value;}}
	public bool 				IsFrozen			{get{return m_isFrozen;} set{m_isFrozen = value;}}

	public Enums.DIRECTION 		FillDirection		{get{return m_fillDir;} set{m_fillDir = value;}}

	// Game Rules & Experiments
	public bool AllowDiagCards		{get{return m_allowDiagCards;} 	set{m_allowDiagCards = value;}}
	public bool AllowDiagSuites		{get{return m_allowDiagSuites;} set{m_allowDiagSuites = value;}}
	public bool BackTrackEnabled	{get{return m_isBacktrackEnabled;} set{m_isBacktrackEnabled = value;}}
	public bool MiniHandValid		{get{return m_miniHandsValid;} 	set{m_miniHandsValid = value;}}
	public bool FullHandValid		{get{return m_fullHandsValid;} 	set{m_fullHandsValid = value;}}
	public bool CardLoopAround		{get{return m_cardValLoopAround;} set{m_cardValLoopAround = value;}}
	public bool HighCardValid		{get{return m_HighCardValid;} set{m_HighCardValid = value;}}
	public bool SuitsCreateCards	{get{return m_suitsCreateCards;} set{m_suitsCreateCards = value;}}
	public bool SuitsZapCard		{get{return m_suitsZapCards;} set{m_suitsZapCards = value;}}

	public int SuitSpawnRate 		{get{return suitSpawnRate;} set{suitSpawnRate = value;}}
	public int MaxMulligans			{get{return maxMulligans;} set{maxMulligans = value;}}
	public int MulligansUsed		{get{return mulligansUsed;} set{mulligansUsed = value;}}


	public bool ReuseBurntCard		{get{return m_reuseBurntCard;} set{m_reuseBurntCard = value;}}
	public bool ReuseZappedCard		{get{return m_reuseZappeCard;} set{m_reuseZappeCard = value;}}
	public bool SwapOnBoardCard		{get{return m_swapBoardCard;} set{m_swapBoardCard = value;}}
	public int  MaxSpawnCards		{get{return maxCardsOnBord;} set{maxCardsOnBord = value;}}


	//This menthod generates a new deck
	public int InitNewDeckData()
	{

		DeckData tDeck 		= ScriptableObject.CreateInstance("DeckData") as DeckData;
		tDeck.GenerateCards(m_decks.Count);
		
		//		Enums.SUIT [] suitArr = new Enums.SUIT[4] {Enums.SUIT.CLUB,Enums.SUIT.SPADE,Enums.SUIT.CLUB,Enums.SUIT.SPADE};
		//		tDeck.GenerateCustomCards(m_decks.Count,suitArr);

		m_decks.Add(tDeck);

		return Decks.Count;
	}

	public void CleanCurrentGame()
	{
		HandsPlayed.Clear ();

		if(m_decks != null)
		{
			m_decks.Clear ();
		}

		m_currDeckID 		= 0;
		ProgressLevel 		= 0;

		GameInfoContainer.Instance.Clean ();
		GameBoard.Instance.CleanGameBoard ();

		IsStarted			= false;
		IsFinished			= true;
		IsPaused			= false;
		MiniHandsCount 		= 0;
		FullHandsCount		= 0;
		CoinWonInGame 		= 0;
	}

	void InitGameModeSettings()
	{
		// Decide these based on the size of the each row 
		//(1.0f - (Mathf.Abs(MaxCols - GridColumns)) * 0.1f);
		//(1.0f - (Mathf.Abs(MaxRows - GridRows)) * 0.1f);

		ProgressItem selLevel = StartView.Instance.CurrPItem;


		if(selLevel != null)
		{
			GameMode 			= selLevel.GameMode;
			GameView.Instance.SetUpUI (selLevel);
			
			//GamePlay			= selLevel.GamePlay;
			GameDiff			= selLevel.diff;

			GridRows 			= selLevel.GridRows;
			GridColumns 		= selLevel.GridColumns;

			m_totalTimeSec		= selLevel.StartTime;
			m_buffTimeLeft 		= selLevel.BuffTimeLeft;
			m_totalNumMoves 	= selLevel.startMoves;
			MaxSpawnCards 		= selLevel.MaxSpawnCards;

			//Deck variants
			ReuseBurntCard		= selLevel.ReuseBurntCard;
			ReuseZappedCard		= selLevel.ReuseZappedCard;
		}
	
		
		// Drag and Select Rules
		MaxMulligans 		= 3;
		AllowDiagCards 		= true;//(GameMode == Enums.GAMEMODE.RELAY);			// false >> More ZAPS & SWAPS , true >> Less ZAPS & SWAPS
		AllowDiagSuites 	= AllowDiagCards;
		BackTrackEnabled	= true;
		FillDirection		= Enums.DIRECTION.SOUTH;

		//Hand related Rules
		HighCardValid 			= false;
		MiniHandValid			= true;
		FullHandValid			= true;
		CardLoopAround			= true;
		SuitSpawnRate			= 2;

		SuitsCreateCards 		= true;
		SuitsZapCard 			= true; 
		SwapOnBoardCard 		= true;
	}

	public void StartGame ()
	{
		CleanCurrentGame ();

		boardXRes 			= 1.0f;
		boardYRes 			= 1.15f;

		InitGameModeSettings ();

		m_totalScore		= 0;
		m_totalXP			= 0;
		MulligansUsed		= 0;

		boardColOffset 		= Mathf.Max(1.1f, 0.9f + (Mathf.Abs(C.ABS_MAX_COLS - GridColumns + 1)) * 0.5f);
		boardRowOffset 		= (0.25f + (Mathf.Abs(C.ABS_MAX_ROWS - GridRows + 1)) * 0.5f);

		m_timeLeftInSec		= m_totalTimeSec;

		IsFinished 			= false;
		IsPaused 			= false;
		IsBuffed 			= false;
		IsFrozen 			= false;
		IsStarted			= true;

		m_startDecks 		= C.START_DECK_PCK;
		m_decks 			= new List<DeckData>();

		// Lets generate Decks
		for (int i= 0; i<m_startDecks;++i)
		{
			InitNewDeckData();
		}

		m_currDeckID 		= 0;
	}


	public void UpdateGameTimers()
	{
		if(!IsFinished && !IsPaused)
		{	
			BuffTimeLeft -= Time.deltaTime;
			
			if(BuffTimeLeft <= 0)
			{
				BuffTimeLeft = 0;
				OnBuffTimeFinish();

				if(IsFrozen && FreezeTimeLeft > 0)
				{
					FreezeTimeLeft -= Time.deltaTime;

					if(FreezeTimeLeft <= 0)
					{
						FreezeTimeLeft = 0;
						OnFreezeTimeFinish();
					}
				}else
				{
					TimeLeft -= Time.deltaTime;
					if(TimeLeft <=0)
					{
						TimeLeft = 0.0f;
						CheckAndNotifGameOver();
					}
				}
			}
		}
	}

	public bool CanUseMulligan()
	{
		return MulligansUsed < MaxMulligans;
	}

	public void SetGamePause(bool pause)
	{
		IsPaused = pause;

		if (IsPaused) 
		{
			GameBoard.Instance.ClearSelectedObjects();
			GameBoard.Instance.ToggleConnector(true);
			GameBoard.Instance.HideContainer();
		}
		else
		{
			GameBoard.Instance.ToggleConnector(false);
			GameBoard.Instance.ShowContainer();
		}
	}

	public bool UseMulligan()
	{
		if(CanUseMulligan())
		{
			if(GameMode == Enums.GAMEMODE.TIMED)
			{
				TimeLeft += 30;
			}
			else if(GameMode == Enums.GAMEMODE.MOVES)
			{
				TotalNumMoves += 10;
			}

			MulligansUsed -= 1;

			return true;
		}

		return false;
	}
	
	public void OnBuffTimeFinish()
	{
		if(!IsBuffed)
		{
			IsBuffed = true;
			GameBoard.Instance.CreateEmptyGrid();
			GameBoard.Instance.FillBoardEmptySpaces ();
			//SoundMgr.Instance.PlayUISound (Enums.UISoundID.UI_Congrats);
		}
	}

	public void OnFreezeTimeFinish()
	{
		if(IsFrozen)
		{
			IsFrozen = false;
		}
	}

	public string HandsInfo()
	{
		if (GameMode == Enums.GAMEMODE.MOVES)
			return "Hands:" + MovesLeft;
		else
			return "";
	}


	public bool IsValidHandSize(List<BaseBoardObject> cards)
	{
		if((MiniHandValid && cards.Count == C.MINI_HAND_SIZE) ||  cards.Count == C.FULL_HAND_SIZE)
			return true;

		return false;
	}

	public HandData CreateHandFromSelected(List<BaseBoardObject> selObjs,bool allCards)
	{
		Enums.HTYPE handType 		= Enums.HTYPE.INVALID;
		Enums.MHTYPE minHandType 	= Enums.MHTYPE.INVALID;

		HandData tHand 				= null;

		if(allCards && IsValidHandSize(selObjs))
		{
			tHand = ScriptableObject.CreateInstance("HandData") as HandData;
			tHand.AddCards (selObjs);
			tHand.ComputeHandType();

			handType 		= tHand.HandType;
			minHandType		= tHand.MiniHandType;
			bool handPlayed	= false;

			if(tHand != null)
			{
				foreach(HandData playedhand in HandsPlayed)
				{
					handPlayed = playedhand.IsEqual(tHand);
					if(handPlayed)
					{
						FeedbackContainer.Instance.ShowErrorFeedback("Hand Played Before");
						handPlayed = true;
					} 
				}

				if(handType == Enums.HTYPE.INVALID && minHandType == Enums.MHTYPE.INVALID)
				{
					FeedbackContainer.Instance.ShowErrorFeedback(tHand.HandTypeString);
				}
				else
				{
					Game.Instance.HandsPlayed.Add (tHand);

					int hScore 		= tHand.ComputeScore();
					int hXP 		= tHand.ComputeXP();
					int hTBonus 	= tHand.ComputeBonus();

					//Debug.Log(">> XP + "+hXP);

					//int htCoins 	= tHand.ComputeCoins();

					if(handPlayed)
					{
						TotalScore	+= hScore;
						TotalXP 	+= hXP;
					}
					else
					{
						TotalScore	+= hScore;
						TotalXP 	+= hXP;
					}

					if(tHand.IsMini)
						MiniHandsCount += 1;
					else
						FullHandsCount += 1;

					UpdateBonus(hTBonus);
					FeedbackContainer.Instance.ShowHandFormedInfo(tHand);
				}
			}
		}
		else if(allCards && !IsValidHandSize(selObjs))
		{
			if(MiniHandValid && !FullHandValid)
				FeedbackContainer.Instance.ShowErrorFeedback("Select 3 Cards");
			else if(!MiniHandValid && FullHandValid)
				FeedbackContainer.Instance.ShowErrorFeedback("Select 5 Cards");
			else if(MiniHandValid && FullHandValid)
				FeedbackContainer.Instance.ShowErrorFeedback("Select 3 or 5 Cards");

			SoundMgr.Instance.PlayVoiceFeedback(Enums.FEEDBACK_SND.NO);
		}
		else
		{
			// This is a Suit Hand
			SuitHandsCount  += 1;
			
			foreach(BaseBoardObject _obj in selObjs)
			{
				_obj.HandScore 	= 1;
				TotalScore 		+= _obj.HandScore;
			}
			
			int bonus 		= Mathf.Min (selObjs.Count,C.MAX_SUITTIME);
			UpdateBonus(bonus * 2);
			
			if(selObjs.Count == 4)
				SoundMgr.Instance.PlayVoiceFeedback(Enums.FEEDBACK_SND.GOOD);
			else if(selObjs.Count == 5)
				SoundMgr.Instance.PlayVoiceFeedback(Enums.FEEDBACK_SND.GREAT);
			else if(selObjs.Count == 6)
				SoundMgr.Instance.PlayVoiceFeedback(Enums.FEEDBACK_SND.EXCELLENT);
			else if(selObjs.Count > 6)
				SoundMgr.Instance.PlayVoiceFeedback(Enums.FEEDBACK_SND.AMAZING);
		}


		return tHand;
	}


	public void UpdateBonus(int bonus)
	{
		if(GameMode == Enums.GAMEMODE.TIMED)
		{
			float div 		= (ProgressLevel + 1);
			float dBonus	= (float)bonus/div;

			//Debug.Log ("Bonus : "+bonus + " dBonus: "+dBonus);

			CurrBonusTime	+=  dBonus;
			
			if(CurrBonusTime >= NextFullBonusTime)
			{
				ProgressLevel		+=1;
				NextFullBonusTime	=C.LEVEL_TIME_STEP;
				TimeLeft			+= NextFullBonusTime;
				CurrBonusTime 		= 0;
			}

			GameInfoContainer.Instance.timerContainer.UpdateBonusBar(CurrBonusTime,NextFullBonusTime);
		}
	}

	
	public void CheckAndNotifGameOver()
	{
		if(Game.Instance.GameMode == Enums.GAMEMODE.TIMED && TimeLeft <=0  || Game.Instance.GameMode == Enums.GAMEMODE.MOVES && MovesLeft <= 0)
		{
			// Offer Add Time or Add Cards
			// For Now End the Game
			GameBoard.Instance.ClearSelectedObjects();
			FeedbackContainer.Instance.ResetFeedbackContainer();

			GameBoard.Instance.ToggleConnector(true);
			Game.Instance.SetGamePause(true);
			IsFinished 	= false;

			GameInfoContainer.Instance.timerContainer.buffTimerTxt.text = "GameOver";

			//PopupMgr.Instance.ShowPopContainer(Enums.POPUP_TYPE.LEVEL,Enums.POPUP_TAB.MULLIGAN);
		}
	}

	public void FinishTheGame()
	{
		IsFinished 	= true;
		CleanCurrentGame ();
	}

#region BOOST related

	public void FreezeGame()
	{
		IsFrozen 		= true;
		FreezeTimeLeft  += C.FREEZE_TIME;
	}


#endregion

#region DECK RELATED

	// Deck related
	int CardsLeftToBurn()
	{
		int cardsLeft = 0;

		foreach(DeckData _deck in m_decks)
		{
			cardsLeft += _deck.CardCount;
		}

		return cardsLeft;
	}

	public int CardsLeft()
	{
		return 0;
	}

	public DeckData CurrDeck()
	{
		if(CurrDeckID< Decks.Count)
			return Decks [CurrDeckID] as DeckData;
		else
			return null;
	}

	public string DeckInfo()
	{
		if(CurrDeck() != null)
			return ""+ (CurrDeckID + 1) + "["+ CurrDeck().CardCount +"]" + " / "+(Decks.Count -1);
		else
			return "";
	}

#endregion

}
