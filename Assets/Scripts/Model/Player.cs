﻿using UnityEngine;
using System.Collections;

public class Player  
{
	public enum THEME
	{
		JELLY 	= 0,
		JELLY2 	= 1,
		RECT	= 2,
		MAXTHEME
	}

	static Player m_Instance;

	int m_CurrLevel				= 0;
	int m_TotalStars			= 0;

	public static THEME Theme	= THEME.RECT;
 	public static Player Instance
	{
		get
		{
			if (m_Instance == null)
			{
				m_Instance	= new Player();
			}
			return m_Instance;
		}
	}

	public int Level {get{return m_CurrLevel;} set{m_CurrLevel = value;}}
	public int Stars {get{return m_TotalStars;} set{m_TotalStars = value;}}
}
