﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeckData : ScriptableObject {

	List<CardData> m_deckCards	= new List<CardData>();

	public const int MAXCARDS 	= 13;
	public const int MAXSUITS 	= 4;
	const int MAXWILDS  		= 2;

	static int 			id_Count = 0;
	int m_deckID 				= 0;

	//Awake is always called first.. So make sure you use this to create any new
	void Awake()
	{
		// Find the reference to the GameController on the table
		//m_game = GameObject.FindGameObjectWithTag("GameController");
	}

	public int CardCount	{get{return m_deckCards.Count;}}
	public int DeckID		{get{return m_deckID;}}


	public void  GenerateCards(int deckID)
	{
		m_deckID = deckID;

		foreach (Enums.SUIT _suit in (System.Enum.GetValues(typeof(Enums.SUIT))))
		{
			for(int _num = 1; _num <= MAXCARDS; _num ++)
			{
				CardData newCard = ScriptableObject.CreateInstance("CardData") as CardData;
				newCard.Init(_suit,_num,id_Count++,m_deckID);
				m_deckCards.Add(newCard);
			}
		}

		//Debug.Log ("Num Cards generated : " + m_cards.Count);
	}

	public void  GenerateCustomCards(int deckID,Enums.SUIT[] suitTypes)
	{
		m_deckID = deckID;
		
		foreach (Enums.SUIT _suit in suitTypes)
		{
			for(int _num = 1; _num <= MAXCARDS; _num ++)
			{
				CardData newCard = ScriptableObject.CreateInstance("CardData") as CardData;
				newCard.Init(_suit,_num,id_Count++,m_deckID);
				m_deckCards.Add(newCard);
			}
		}
		
		//Debug.Log ("Num Cards generated : " + m_cards.Count);
	}

	public bool AddCardToDeck(CardData cData)
	{
		if(!m_deckCards.Contains(cData))
		{
			m_deckCards.Add(cData);
			//cData.UseCount += 1;
			return true;
		}

		return false;
	}

	public bool RemoveCardFromDeck(CardData cData)
	{
		if(m_deckCards.Contains(cData))
		{
			m_deckCards.Remove(cData);
			return true;
		}
		
		return false;
	}

	public CardData DrawRandCardFromDeck()
	{
		CardData rndCard = null;
		
		if(m_deckCards.Count >=1)
		{
			// we have reached end of this Deck. Switch to New Deck of cards
			int rndIndex = Random.Range (0, m_deckCards.Count);
			rndCard = m_deckCards [rndIndex] as CardData;

			RemoveCardFromDeck(rndCard);

		}

		return rndCard;
	}

	public CardData DrawCardWithInfo(Enums.SUIT suit, int num)
	{
		CardData infoCard = null;

		foreach(CardData cData in m_deckCards)
		{
			if(cData.Num == num && cData.Suit == suit)
			{
				infoCard = cData;
				RemoveCardFromDeck(cData);

				break;
			}
		}

		//rndCard = m_cards [rndIndex] as CardData;
		return infoCard;
	}

	public bool CheckCardInDeck(Enums.SUIT suit, int num)
	{
		bool cardInDeck = false;
		
		foreach(CardData cData in m_deckCards)
		{
			if(cData.Num == num && cData.Suit == suit)
			{
				cardInDeck = true;
				break;
			}
		}
		return cardInDeck;
	}


}
