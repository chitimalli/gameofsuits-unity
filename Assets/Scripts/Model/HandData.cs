using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HandData : ScriptableObject {

	// This class will store a specific hand, and various other helpwr methods required for a specific hand
	// We can also conver this into serializable String

	int[]	m_suitCount = new int[DeckData.MAXSUITS];		// Each one holds the count for each Suit
	int[]	m_numCount 	= new int[DeckData.MAXCARDS+1];

	List<CardData> 	m_handCards 	= new List<CardData>();

	Enums.HTYPE 		m_handType			= Enums.HTYPE.INVALID;	
	Enums.MHTYPE 		m_minihandType		= Enums.MHTYPE.INVALID;	

	int m_handScore 			= -1;
	int m_handXP 				= -1;
	int m_timeBonus				= 0;	// this is the number of seconds you g=cound earn by playing a specific Hand	
	CardData m_highCard			= null;	// Lets default to null
	bool m_isMini				= false;
	string handTypeStr			= "";

	// Use these to determine the Hand type
	int m_numOfPairsCnt 	= 0;		// Gives OnePair and TwoPair
	int m_threeOfAkindCnt 	= 0;		// Gives ThreeOf A Kind
	int m_fourOfAkindCnt 	= 0;		// Gives 4 of a Kind
	bool m_isFlush			= false;	// There can be one Flush in a given set of cards
	bool m_isStraight		= false;	// There can be one Straight in a given set of Cards
	bool m_isRoyalSet 		= true;		// Are the cards Royal


	//Public accessor functions for internal variables
	public Enums.HTYPE  	HandType 		{get{return m_handType;} set{m_handType = value;}}
	public Enums.MHTYPE  	MiniHandType 	{get{return m_minihandType;} set{m_minihandType = value;}}
	public List<CardData> 	Cards			{get{return m_handCards;}}
	public int[]			SuitCountArray	{get{return m_suitCount;}}
	public int[]			NumCountArray	{get{return m_numCount;}}
	public int				HandScore		{get{return m_handScore;} set{m_handScore = value;}}
	public int				HandXP			{get{return m_handXP;} set{m_handXP = value;}}
	public int				HandTimeBonus	{get{return m_timeBonus;} set{m_timeBonus = value;}}
	public bool				IsMini			{get{return m_isMini;} set{m_isMini = value;}}
	public string 	HandTypeString			{get{return handTypeStr;} set{handTypeStr = value;}}


	bool AddCard(CardData _c)
	{
		if (m_handCards.Contains (_c))
			return false;
		else
		{
			m_handCards.Add(_c);
			UpdateCountForCardBy(_c,1);
		}

		return true;
	}

	bool RemoveCard(CardData _c)
	{
		if (m_handCards.Contains (_c))
		{
			m_handCards.Remove(_c);
			UpdateCountForCardBy(_c,-1);
			return true;
		}
	
		return true;
	}

	void UpdateCountForCardBy(CardData _card,int _val)
	{
		// Update the NumCount Array
		NumCountArray [_card.Num] += _val;
		
		// Update the Suit count
		SuitCountArray [(int)_card.Suit] += _val;
	}

	bool IsStraight()
	{
		// CheckIfItIsAStrtaight 
		//[We cannot have a straight if we have a pair, 3 or 4 of a kind]
		if((m_numOfPairsCnt + m_threeOfAkindCnt +  m_fourOfAkindCnt) == 0 )
		{
			// Lets Sort and find the Diff btw each number
			List<int>	tCardInfo = new List<int>();
			foreach(CardData _c in Cards)
			{
				if(m_highCard == null || m_highCard.Num > _c.Num)
					m_highCard = _c;

				//Debug.Log(" >>>>> St check Adding "+_c.Num);
				tCardInfo.Add(_c.Num);
			}

			//We Do have an exception, of A.. A could be counted as 1, or 14 based other cards
			//The only possible combination with A as 13 is [10,J,Q,K,A] == ROYAL_Cards
			if(!m_isRoyalSet)
			{
				tCardInfo.Sort();

				int oldCVal = (int) tCardInfo[0];

				foreach(int cVal in tCardInfo)
				{
					int diff = Mathf.Abs(cVal - oldCVal);

					if(diff > 1)
					{
						return false;
					}
					
					oldCVal = cVal;
				}
			}

			return true;
		}
		
		return false;
	}

	public void AddCards(List<BaseBoardObject> _cards)
	{
		foreach (BaseBoardObject c in _cards)
		{
			if(c.CompareTag(C.CARDOBJ))
				AddCard (((CardObject)c).Card);
		}

		// Set the Hand type to Mini
		IsMini = (m_handCards.Count == C.MINI_HAND_SIZE);
	}

	public int ComputeScore()
	{
		// If the score was already calculated for this Hand
		if(HandScore > 0)
			return HandScore;

		int cardScore = 0;

		foreach(CardData card in m_handCards)
			cardScore += card.Num;

		int lenMul		 = 1;
		int handTypeVal  = 1;

		if(IsMini)
		{
			lenMul = 3;
			handTypeVal = (int)MiniHandType + 1;
		}else
		{
			lenMul = 5;
			handTypeVal = (int)HandType + 3;
		}

		//Debug.Log ("*** hand typeVal " + handTypeVal);

		HandScore = (handTypeVal * lenMul) + cardScore;

		return HandScore;
	}

	public int ComputeXP()
	{
		// If the score was already calculated for this Hand
		if(HandXP > 0)
			return HandXP;
		
		// Do the Math for all score
		// Consider Suit, Boost, time, bonus etc.
		
		int multiplier = IsMini ? 3 : 5;
		
		int handTypeVal = (int)HandType + 1;
		HandXP = handTypeVal * multiplier;

		return HandXP;
	}

	public int ComputeBonus ()
	{
		HandTimeBonus = 0;
		
		if(IsMini && MiniHandType != Enums.MHTYPE.INVALID)
		{
			HandTimeBonus 	= (int)MiniHandType * Cards.Count;
		}
		else
		{
			HandTimeBonus 	= (int)HandType * Cards.Count;
		}

		return HandTimeBonus;
		// This will calculate any bonus that needs to be given to the player
	}

	public int ComputeCoins ()
	{
		HandTimeBonus = 0;
		
		if(IsMini && MiniHandType != Enums.MHTYPE.INVALID)
		{
			HandTimeBonus 	= ((int)MiniHandType + 1) * m_handCards.Count;
		}
		else
		{
			HandTimeBonus 	= ((int)HandType + 1) * m_handCards.Count;
		}
		
		return HandTimeBonus;
		// This will calculate any bonus that needs to be given to the player
	}

	public void ComputeHandType()
	{

		// This code is to verify the Hands formed
		for (int numkey = 1; numkey< NumCountArray.Length;numkey++)
		{
			if(NumCountArray[numkey] > 0)
			{
				if(NumCountArray[numkey] == 2)
					m_numOfPairsCnt	+=1;
				if(NumCountArray[numkey] == 3)
					m_threeOfAkindCnt	+=1;
				if(NumCountArray[numkey] == 4)
					m_fourOfAkindCnt	+=1;
			}

			// If this is never hit that means we have a RoyalSet in the  [10,J,Q,K,A]
			// For Mini Hand it will have to be [A,J,Q,K]
			if(IsMini && numkey > 1 && numkey <10 && NumCountArray[numkey] >0)
				m_isRoyalSet = false;
			else if(numkey > 1 && numkey <9 && NumCountArray[numkey] >0)
				m_isRoyalSet = false;

				//Debug.Log ("##### *** Num count for "+ numkey +" is: "+NumCountArray[numkey]);
		}

		for (int suitkey = 0; suitkey< SuitCountArray.Length;suitkey++)
		{
			// Might have to get the 5 based on the game mode, if MiniMode
			if(SuitCountArray[suitkey] == 5 || (IsMini && SuitCountArray[suitkey] == 3))
			{
				m_isFlush = true;
			}
				//Debug.Log ("*** Suit count for "+ suitkey +" is: "+SuitCountArray[suitkey]);
		}

		m_isStraight = IsStraight();

		MiniHandType 	= Enums.MHTYPE.INVALID;
		HandType 		= Enums.HTYPE.INVALID;
		HandTypeString = "Invalid Set";

		// START LOOKING from TOP TO BOTTOM OF THE HANDTYPE
		if(!IsMini && m_isRoyalSet && m_isFlush)
		{
			HandType = Enums.HTYPE.ROYAL_FLUSH;
			HandTypeString = "Royal Flush";
		}
		else if(m_isStraight && m_isFlush)
		{
			if(IsMini)
			{
				MiniHandType = Enums.MHTYPE.M_STRAIGHT_FLUSH;
				HandTypeString = "Mini Straight Flush";
			}
			else
			{
				HandType = Enums.HTYPE.STRAIGHT_FLUSH;
				HandTypeString = "Straight Flush";
			}
		}
		else if(!IsMini && m_fourOfAkindCnt >0)
		{
			HandType = Enums.HTYPE.FOUR_OFA_KIND;
			HandTypeString = "Four Of A Kind";

		}
		else if(!IsMini && m_threeOfAkindCnt == 1 && m_numOfPairsCnt == 1)
		{
				HandType = Enums.HTYPE.FULL_HOUSE;
				HandTypeString = "Full House";
		}
		else if(m_isFlush)
		{
			if(IsMini)
			{
				MiniHandType = Enums.MHTYPE.M_FLUSH;
				HandTypeString = "Mini Flush";
			}
			else
			{
				HandType = Enums.HTYPE.FLUSH;
				HandTypeString = "Flush";
			}
		}
		else if(m_isStraight)
		{
			if(IsMini)
			{
				MiniHandType = Enums.MHTYPE.M_STRAIGHT;
				HandTypeString = "Mini Straight";
			}
			else
			{
				HandType = Enums.HTYPE.STRAIGHT;
				HandTypeString = "Straight";
			}
		}
		else if(m_threeOfAkindCnt == 1)
		{
			if(IsMini)
			{
				MiniHandType = Enums.MHTYPE.M_THREE_OFA_KIND;
				HandTypeString = "Mini Three Of A Kind";
			}
			else
			{
				HandType = Enums.HTYPE.THREE_OFA_KIND;
				HandTypeString = "Three Of A Kind";
			}
		}
		else if(!IsMini && m_numOfPairsCnt == 2)
		{
			HandType = Enums.HTYPE.TWO_PAIR;
			HandTypeString = "Two pair";
		}
		else if(m_numOfPairsCnt == 1)
		{
			if(IsMini)
			{
				MiniHandType = Enums.MHTYPE.M_PAIR;
				HandTypeString = "Mini Pair";
			}
			else
			{
				HandType = Enums.HTYPE.ONE_PAIR;
				HandTypeString = "One Pair";
			}
		}
		else if(!IsMini && Game.Instance.HighCardValid)
		{
			HandType = Enums.HTYPE.HIGH_CARD;
			HandTypeString = "High Card";
		}
		else 
			HandType = Enums.HTYPE.INVALID;
	}

	// Compare a specic Hand of cards with another Hand
	public bool IsEqual(HandData _hand,bool _deckCheck = false)
	{

		if(_hand.GetInstanceID() == this.GetInstanceID())
			return true;

		if(_hand.Cards.Count != Cards.Count)
			return false;

		int contCount = 0;
		foreach(CardData _card in Cards)
		{
			foreach(CardData _pCard in _hand.Cards)
			{
				if(_pCard.IsEqual(_card,_deckCheck)) contCount++;
			}
		}

		if(contCount == Cards.Count)
			return true;

		return false;
	}

}
