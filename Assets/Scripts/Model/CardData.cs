﻿using UnityEngine;
using System.Collections;

public class CardData : ScriptableObject {

	Enums.SUIT m_suit;
	 string m_suitName;

	int m_num;
	bool m_isWild;

	int m_cardID;	// This will be unique to each Card/ Like a serial number in a Deck
	int m_deckID;	// The Deck this Card belongs to
	int m_useCount;	// The Deck this Card belongs to
	int m_bonusMul;

	public int TRowID;
	public int TColID;

	// Accessory Functions
	public Enums.SUIT Suit		{ get{	return m_suit;}} 
	public int Num 				{ get{ return m_num;}}
	public bool IsWild 			{ get{ return m_isWild;}}
	public int	CardID			{ get{	return m_cardID;}}
	public int	UseCount		{ get{	return m_useCount;} set{m_useCount = value;}}
	public int	BonusMul		{ get{	return m_bonusMul;} set{m_bonusMul = value;}}
	public int 	DeckID			{ get{	return m_deckID;} set{m_deckID = value;}}

	public static string SNum(int num)
	{ 
		string retVal = num.ToString();
		
		if(num == 1)	retVal = "A";
		if(num == 11)	retVal = "J";
		if(num == 12)	retVal = "Q";
		if(num == 13)	retVal = "K";
		
		return retVal;		
	}

	public void Init(Enums.SUIT _suit, int num)
	{
		m_suit 		= _suit;
		m_num 		= num;
		m_isWild 	= false;

		UseCount	= 0;
		BonusMul	= 1;
		TRowID	 	= 0;
		TColID 		= 0;
	}

	public void Init(Enums.SUIT _suit, int num,int cID, int dID)
	{
		Init (_suit,num);

		m_cardID 	= cID;
		m_deckID 	= dID;

	}

	public bool IsEqual(CardData cData, bool deckCheck)
	{
		if(deckCheck)
			return (cData.DeckID == DeckID && cData.Num == m_num && cData.Suit == m_suit);
		else
			return (cData.m_num == m_num && cData.m_suit == m_suit);
	}
}

