using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameBoard : FFUIContainer {
	#region static
	public static GameBoard Instance;

	List<BaseBoardObject>	selectHandList 	= new List<BaseBoardObject>();
	public GameObject		selCardHolder;

	public static Vector3 RowColPos(int row,int col,int _z = 0)
	{
		Vector3 cardPos	= new Vector3(col + Game.boardColOffset , (row * Game.boardYRes)+ Game.boardRowOffset , _z);
		return cardPos;
	}
	
	public static Vector3 TempPos(int row,int col, int _z = 0)
	{
		int tdRow	= 1;
		int tdCol	= 1;

		int tCol 	= col;
		int tRow 	= row;

		if(Game.Instance.FillDirection == Enums.DIRECTION.SOUTH)
		{
			tRow 	= Game.GridRows + tdRow;

		}else if(Game.Instance.FillDirection == Enums.DIRECTION.NORTH)
		{
			tRow 	= -tdRow;

		}else if(Game.Instance.FillDirection == Enums.DIRECTION.WEST)
		{
			tCol 	= Game.GridColumns + tdCol;

		}else if(Game.Instance.FillDirection == Enums.DIRECTION.EAST)
		{
			tCol 	= -tdCol;
		}

		Vector3 tPos	= new Vector3(tCol + Game.boardColOffset ,  (tRow * Game.boardYRes) + Game.boardRowOffset , _z);

		return tPos;
	}


	#endregion

	List<BaseBoardObject>	m_listOfBoardObjects 	= new List<BaseBoardObject>();
	List<BaseBoardObject> 	m_listOfSelectedObjects = new List<BaseBoardObject>();

	List<BaseBoardObject> 	m_gridObjs = new List<BaseBoardObject>();

	public List<BaseBoardObject>		AllBoardObjects		{get{return m_listOfBoardObjects;}}
	public List<BaseBoardObject>		SelectedObjects		{get{return m_listOfSelectedObjects;}}

	LineRenderer 	m_line;
	bool hideLine 	= false;
	GameObject[] m_columnArray;
	GameObject	m_boardGrid;


	// AllBoardObj Info
	int allCardsOnBoard 	= 0;
	int allSuitsOnBoard 	= 0;

	int clubSuitsOnBoard 	= 0;
	int spadeSuitsOnBoard 	= 0;
	int heartSuitsOnBoard 	= 0;
	int diamondSuitsOnBoard = 0;
	int emptyOnBoard 		= 0;

	// AllSelected Info

	bool SelAllCardObj 		= true;
	bool SelAllSuitObj 		= true;
	bool SelAllCoinObj 		= true;

	bool SelAllSameSuit 	= true;
	int SelCardCount 		= 0;
	int SelAllCardVal 		= 0;
	
	BaseBoardObject SelFirstObj 	= null;
	BaseBoardObject SelLastObj 	= null;
	
	int newR = 0;
	int newC = 0;

	public bool DisableTouch = false;


	#region GetSet

	#endregion

	void Awake () 
	{
		Instance = this;

		m_line = gameObject.GetComponent<LineRenderer> ();
		m_columnArray = null;
	}

	public override void ShowContainer()
	{
		gameObject.transform.localScale = showScale;
		SlideToPosition (gameObject, showPos, showTime);
		IsShowing = true;
		OnShow ();
	}
	
	public override void HideContainer()
	{
		gameObject.transform.localScale = hideScale;
		SlideToPosition (gameObject, hidePos,showTime);
		IsShowing = false;
		OnHide ();
	}

	public void CleanGameBoard()
	{
		foreach(BaseBoardObject _card in AllBoardObjects)
		{
			Destroy(_card.gameObject);
		}	


		foreach(BaseBoardObject _eObj in m_gridObjs)
			Destroy(_eObj.gameObject);


		AllBoardObjects.Clear ();
		SelectedObjects.Clear ();
		m_gridObjs.Clear ();

		m_columnArray 	= null;
		m_boardGrid 	= null;
	}


	public void CreateEmptyGrid()
	{
		m_boardGrid = new GameObject("boardGrid");
		m_boardGrid.transform.parent = transform;
		for ( int col = 0; col < Game.GridColumns; col++)
		{
			for ( int row = 0; row < Game.GridRows; row++)
			{
				CreateEmptyGameObjAt(row, col);
			}
		}

		if(m_columnArray == null)
		{
			m_columnArray = new GameObject[Game.GridColumns];
			for ( int col = 0; col < Game.GridColumns; col++)
			{
				GameObject _colObj 			= new GameObject();
				_colObj.name				= "Col_" + (col+1).ToString("00");
				_colObj.transform.parent 	= transform;
				m_columnArray [col] 			= _colObj;
			}
		}
	}

	public void FillBoardEmptySpaces()
	{
		//Creating the gems from the list of gems passed in parameter
		for ( int col = 0; col < Game.GridColumns; col++)
		{
			for ( int row = 0; row < Game.GridRows; row++)
			{
				BaseBoardObject obj = BoardObjectAt(row,col) ;

				// This means we got to creata a new Object here
				if(obj == null || (obj != null && obj.IsDirty))
				{
					int pickType = Random.Range(0,11);

					float falltime = (row +col) * 0.1f + 0.3f;

					if(pickType == 3) //(pickType  % 3 == 0)
					{
						CreateCoinObjAt(row,col,falltime);
					}
					else
					if (pickType % 7 == 0 &&  allCardsOnBoard < Game.Instance.MaxSpawnCards)
					{
						CreateCardAt (row, col,false,null,falltime);
					}
					else
					{
						Enums.SUIT randSuit = Enums.RandSuit();
						CreateSuitAt(row,col,randSuit,false,falltime);
					}
				}
			}
		}

		StartCoroutine (C.DelayAction (0.25f, () => {
			DisableTouch = false;
		}));

	}

	public int SuitCountFor(Enums.SUIT suit)
	{
		if(suit == Enums.SUIT.CLUB) return clubSuitsOnBoard ;
		if(suit == Enums.SUIT.SPADE) return spadeSuitsOnBoard;
		if(suit == Enums.SUIT.DIAMOND) return diamondSuitsOnBoard;
		if(suit == Enums.SUIT.HEART) return heartSuitsOnBoard;

		return 0;
	}

	public bool MaxCardsReached()
	{
		// !SelAllCardObj
		bool maxCardsReached = SelAllSuitObj && (allCardsOnBoard >= Game.Instance.MaxSpawnCards);

		if(maxCardsReached )
		{
			FeedbackContainer.Instance.ShowErrorFeedback("MAX Cards on Board");
		}

		return maxCardsReached;
	}

	public void CreateEmptyGameObjAt(int newR,int newC)
	{
		BaseBoardObject emptyObj	= PFLoader.Instance.EmptyPrefabObj;
		
		if(emptyObj != null && emptyObj.CompareTag(C.EMPTYOBJ))
		{
			emptyObj.SetRowColZID(newR,newC,C.EMPTY_Z);
			emptyObj.SetPosition(RowColPos (newR,newC,emptyObj.ZOrder));
			emptyObj.transform.parent   	= m_boardGrid.transform;
			m_gridObjs.Add(emptyObj);
		}
	}

	static int stackCnt = 0;
	public SuitObject CreateSuitAt(int newR,int newC,Enums.SUIT suit,bool spawnInPlace = false, float _time = 0.5f)
	{
		spawnInPlace 					= (Game.Instance.FillDirection == Enums.DIRECTION.NEWS) ? true : spawnInPlace;

		GameObject _colObj 				= m_columnArray[newC];
		SuitObject suitP				= PFLoader.Instance.SuitsPrefabObj;

		if(suitP != null && suitP.CompareTag(C.SUITOBJ))
		{
			suitP.Suit 					= suit;
			suitP.SetRowColZID(newR,newC,C.CARD_Z);


			if(!spawnInPlace)
			{
				suitP.SetPosition(TempPos(newR,newC,suitP.ZOrder));
				suitP.JumpToPosition(RowColPos (newR,newC,suitP.ZOrder), _time);
				suitP.RotateOnce ();
			}
			else
			{
				suitP.SetPosition(RowColPos (newR,newC,suitP.ZOrder));
				suitP.SpawnInPlace();
			}

			SoundMgr.Instance.PlayChipSound(Enums.CHIP_SND.STACK,stackCnt);
			if(++stackCnt >= 6) stackCnt = 0;

			AddToBoardObjects(suitP);
			suitP.transform.parent   = _colObj.transform;
		}

		return suitP;
	}

	public CardObject CreateCardAt(int newR,int newC, bool spawnInPlace = false, CardData cardData = null, float _time = 0.5f)
	{
		spawnInPlace 					= (Game.Instance.FillDirection == Enums.DIRECTION.NEWS) ? true : spawnInPlace;
		GameObject _colObj 				= m_columnArray[newC];
		CardObject cardP				= PFLoader.Instance.CardPrefabObj;

		// If card Data is null, lets pick a random card
		if(cardData == null)
		{
			cardData			 		= Game.Instance.CurrDeck().DrawRandCardFromDeck();
		}

		if(cardP != null && cardP.CompareTag(C.CARDOBJ) && cardData != null)		
		{
			if(cardP.SetCardData(cardData))
			{
				cardP.SetRowColZID(newR,newC,C.CARD_Z);

				if(!spawnInPlace)
				{
					cardP.SetPosition(TempPos(newR,newC,cardP.ZOrder));
					cardP.JumpToPosition (RowColPos (newR,newC,cardP.ZOrder), _time);
				}
				else
				{
					cardP.SetPosition(RowColPos (newR,newC,cardP.ZOrder));
					cardP.SpawnInPlace();
				}

				AddToBoardObjects(cardP);
				cardP.transform.parent   = _colObj.transform;
			}else
			{
				Debug.LogError("** There was an errow assigning CardDate to the Prefab");
			}
		}

		return cardP;
	}
	
	public void CreateCoinObjAt(int newR,int newC,float _time = 0.5f)
	{
		GameObject _coinObj 			= m_columnArray[newC];
		CoinObject coinObj			= PFLoader.Instance.PickupPrefabObj;

		if(coinObj != null && coinObj.CompareTag(C.COINOBJ))
		{
			//Enums.PICKUP randPick = Enums.RandPickup();

			//pickupObj.SetPickupType(randPick);
			coinObj.SetRowColZID(newR,newC,C.CARD_Z);
			coinObj.SetPosition(TempPos(newR,newC,coinObj.ZOrder));
			coinObj.JumpToPosition (RowColPos (newR,newC,coinObj.ZOrder), _time);
			coinObj.RotateOnce ();
			coinObj.PlaySound(Enums.ACTION_SND.DROP);

			AddToBoardObjects(coinObj);
			coinObj.transform.parent   = _coinObj.transform;
		}
	}


	public void ToggleConnector(bool _hide)
	{
		hideLine = _hide;

		if(_hide)
			m_line.SetVertexCount(0);
	}

	// Update is called once per frame
	void Update () 
	{
		Game.Instance.UpdateGameTimers ();
		UpdateLine ();
	}
	
	void UpdateLine()
	{
		if(hideLine)
			return;

		int i = 0;//Game.Instance.CardsSelected.Count;

		float lineZ = 0;

		if(m_line != null && SelectedObjects.Count >= 1)
		{
			BaseBoardObject _first = SelectedObjects[0];
			BaseBoardObject _last  = SelectedObjects[SelectedObjects.Count-1];

			if(_first != null && _last != null)
				m_line.SetColors(_first.GetLineColor(),_last.GetLineColor());

			//We are adding an extra Vertex to show the current Touch pos
			m_line.SetVertexCount(SelectedObjects.Count + 1);
			
			foreach(BaseBoardObject selObj in SelectedObjects)
			{
				lineZ = transform.position.z + 1;
				Vector3 pos = selObj.transform.position;
				Vector3 lpos = new Vector3(pos.x,pos.y,lineZ); // 2 is working.. But 
				m_line.SetPosition(i, lpos);
				i++;
			}

			//Lets add the Mouse/Touch Position
			Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			touchPos.z = lineZ;
			m_line.SetPosition(i, touchPos);
		}
	}

	#region Fall Animations
	public void CheckForZapObject (BaseBoardObject _obj)
	{
		_obj.TrackClickCount();
		
		if(_obj.ZapIt())
		{
			//string forceTag = "";
			if(Game.Instance.ReuseZappedCard && _obj.CompareTag(C.CARDOBJ))
			{
				Game.Instance.CurrDeck().AddCardToDeck(((CardObject)_obj).Card);
			}

			if(_obj.CompareTag(C.COINOBJ))
			{
				BoostMgr.Instance.Coins += 1;
			}
			
			//PFLoader.Instance.ShowZapEffect(_obj.RowID, _obj.ColID);
			PFLoader.Instance.ShowEffect(_obj.RowID, _obj.ColID,Enums.CFX.THUNDER);

			if(BoostMgr.Instance.ApplyZapBoost())
				Game.Instance.TotalNumMoves -=1;

			DisableTouch = true;
			FallInPlace();
		}
	}

	public bool ApplyZip()
	{
		if(Game.Instance.SuitsCreateCards || Game.Instance.SuitsZapCard)
		{
			CardData cData 			= null;
			CardObject cardOnBoard 	= null;
			
			cardOnBoard = CardOnBoardWith(SelFirstObj.Suit, SelAllCardVal);

			if(cardOnBoard != null)// && Game.Instance.SwapOnBoardCard)
			{
				//XXX >> Note: We are having this false, because the "SelAllCardVal", is getting modified in the process
				// We can alos fix it by uing "cardOnBoard.Card.Num"
				AddToSelectedList(cardOnBoard,false);
				cardOnBoard.MarkDirty();
				Game.Instance.CurrDeck().AddCardToDeck(cardOnBoard.Card);
			}

			if(Game.Instance.CurrDeck ().CheckCardInDeck(SelFirstObj.Suit,SelAllCardVal))
			{
				iTween.ScaleTo(SelLastObj.gameObject, iTween.Hash("scale", Vector3.zero, "islocal", true, "time", 0.25f));

				cData 	= 	Game.Instance.CurrDeck ().DrawCardWithInfo (SelFirstObj.Suit,SelAllCardVal);	
				CreateCardAt(newR,newC,true,cData,0.25f);
				
				// We need some Score to be added 
				int cScore = cData.Num * (SelectedObjects.Count);
				Game.Instance.TotalScore += cScore;
			}

			// XXX >> NOTE: DO Not change the order of these lines, and the "false parameter"
			RemoveFromSelectedObject(SelLastObj,false);		// 1

			foreach(BaseBoardObject obj in SelectedObjects) 
			{
				SlideToPosition(obj.gameObject,SelLastObj.transform.position,0.15f);

				StartCoroutine(C.DelayAction(0.25f,()=>{
					obj.MarkDirty();
				}));

			} // 2
			DestroyNRemoveFromBoard(SelLastObj); //3

			DisableTouch = true;
			StartCoroutine(C.DelayAction(0.3f,()=>{
				FallInPlace(); //4
			}));

			return true;
		}

		return false;
	}

	public bool CanSwap()
	{
		UpdateSelObjInfo ();

		if(SelectedObjects.Count == C.MIN_MIX_SEL)
		{
			if((SelFirstObj.CompareTag(C.CARDOBJ) || SelFirstObj.CompareTag(C.SUITOBJ)) 
			   || (SelLastObj.CompareTag(C.CARDOBJ) || SelLastObj.CompareTag(C.SUITOBJ)))
			{
				if(SelAllCardObj || !SelAllSameSuit)
				{
					return true;
				}
			}
			else
			{
				Debug.Log(">> Can Swap");
				return false;
			}
		} 

		return false;
	}

	public bool CanZip()
	{
		UpdateSelObjInfo ();

		// Cannot mix different suit Objects
		if(!SelAllSameSuit || SelAllCardObj)
		{
			//Debug.Log ("Cannot Zap >> All Suits"+SelAllSameSuit + " All Cards "+SelAllCardObj );
			return false;
		}

		return true;
	}
	

	// This method will validate and Create and new Hand
	public void ValidateAndClearSelected()
	{
		// Dont do eny thing if none no cards are selected
		if(SelectedObjects.Count == 0)
			return;

		//Game.Instance.IsSwapMode && 
		if(CanSwap())
		{
			if(BoostMgr.Instance.ApplySwapBoost(SelFirstObj,SelLastObj))
			{
				Game.Instance.TotalNumMoves -=1;
				GameHeroCont.Instance.PlayAnimation("attack");
			}

			return;
		}

		//!MaxCardsReached() && 
		if(CanZip())
		{
			if(ApplyZip())
			{
				Game.Instance.TotalNumMoves -=1;
				GameHeroCont.Instance.PlayAnimation("attack");
			}

			return;
		}

		if(SelAllSameSuit || SelAllCardObj)
		{
			HandData tHand = Game.Instance.CreateHandFromSelected (SelectedObjects, SelAllCardObj);

			if(tHand != null && (tHand.HandType != Enums.HTYPE.INVALID || (Game.Instance.MiniHandValid && tHand.MiniHandType != Enums.MHTYPE.INVALID)))
			{
				BurnSelectedAndFillBoard();
				GameHeroCont.Instance.PlayAnimation("jump");
			}
			else 
				ClearSelectedObjects();
		}else if(SelAllCoinObj)
		{
			int coinVal = SelectedObjects.Count;
			BoostMgr.Instance.Coins += coinVal;
			Debug.Log(">> Awarding a coin value of :  "+coinVal);
			BurnSelectedAndFillBoard();
		}else
		{
			ClearSelectedObjects();
		}
	}

	public void BurnSelectedAndFillBoard()
	{
		Game.Instance.TotalNumMoves -=1;

		foreach (BaseBoardObject obj in SelectedObjects)
		{
			obj.MarkDirty();
		} 

		DisableTouch = true;
		//We start a Ananomouse Couroutine  
		StartCoroutine(C.DelayAction(0.15f, () =>{
				FallInPlace();
		}));
	}
	

	public void FallInPlace()
	{
		foreach (BaseBoardObject _selObj in SelectedObjects)
		{
			foreach (BaseBoardObject _boardObj in AllBoardObjects)
			{
				bool sameCol = (_selObj.ColID == _boardObj.ColID);
				bool sameRow = (_selObj.RowID == _boardObj.RowID);

				if (_selObj.IsDirty && !_boardObj.IsDirty)
				{
					if(sameCol || sameRow)
					{
						if(Game.Instance.FillDirection == Enums.DIRECTION.SOUTH)
						{
							if(_boardObj.RowID > _selObj.RowID)
								_boardObj.PosDelta += Enums.DirToVec2 (Game.Instance.FillDirection);
						}else if(Game.Instance.FillDirection == Enums.DIRECTION.NORTH)
						{
							if(_boardObj.RowID < _selObj.RowID)
								_boardObj.PosDelta += Enums.DirToVec2 (Game.Instance.FillDirection);
						}else if(Game.Instance.FillDirection == Enums.DIRECTION.WEST)
						{
							if(_boardObj.ColID > _selObj.ColID)
								_boardObj.PosDelta += Enums.DirToVec2 (Game.Instance.FillDirection);
						}else if(Game.Instance.FillDirection == Enums.DIRECTION.EAST)
						{
							if(_boardObj.ColID < _selObj.ColID)
								_boardObj.PosDelta += Enums.DirToVec2 (Game.Instance.FillDirection);
						}
					}
				} 
			}

			DestroyNRemoveFromBoard(_selObj,1.0f);
		}

		ClearSelectedObjects ();

		// Finally Do make the items Fall in place
		foreach(BaseBoardObject _boardObj in AllBoardObjects)
		{
			if(!_boardObj.IsDirty)
			{
				_boardObj.FallDown(0.5f);
			}
		}
		
		FillBoardEmptySpaces ();
	}

	public void ShuffleBoardObjects()
	{

	}

	#endregion


	#region BoardObjects

	public bool CheckForbackTrack(BaseBoardObject _card)
	{
		if(Game.Instance.BackTrackEnabled && SelectedObjects.Contains (_card) && SelectedObjects.Count>=2)
		{
			// If this card is last but one card. Then deselect the Last card
			BaseBoardObject _lastCard 			= SelectedObjects[SelectedObjects.Count-1];
			BaseBoardObject _lastButOneCard 	= SelectedObjects[SelectedObjects.Count-2];
			
			if(_lastButOneCard != null && _lastCard != null && _card.Equals(_lastButOneCard))
			{
				RemoveFromSelectedObject(_lastCard);
				return true;
			}
		}
		return false;
	}
	
	public void UpdateNeighboursNCardCount()
	{
		allCardsOnBoard 	= 0;
		allSuitsOnBoard 	= 0;
		clubSuitsOnBoard 	= 0;
		spadeSuitsOnBoard 	= 0;
		heartSuitsOnBoard 	= 0;
		diamondSuitsOnBoard = 0;
		emptyOnBoard		= 0;

		foreach(BaseBoardObject obj in AllBoardObjects)
		{
			//obj.UpdateNeighbours(AllBoardObjects);

			if(obj.CompareTag(C.CARDOBJ)) allCardsOnBoard +=1;
			if(obj.CompareTag(C.SUITOBJ))
			{ 
				Enums.SUIT suit = ((SuitObject)obj).Suit;

				if(suit == Enums.SUIT.CLUB) clubSuitsOnBoard += 1;
				if(suit == Enums.SUIT.SPADE) spadeSuitsOnBoard += 1;
				if(suit == Enums.SUIT.DIAMOND) diamondSuitsOnBoard += 1;
				if(suit == Enums.SUIT.HEART) heartSuitsOnBoard += 1;

				allSuitsOnBoard +=1;
			}
			if(obj.CompareTag(C.EMPTYOBJ)) emptyOnBoard +=1;
		}
	}

	void UpdateSelObjInfo()
	{
		SelAllCardObj 	= true;
		SelAllSuitObj 	= true;
		SelAllCoinObj	= true;	

		SelAllSameSuit 	= true;
		SelAllCardVal 	= 0;
		SelCardCount	= 0;
		
		SelFirstObj 	= (SelectedObjects.Count == 0 ) ? null : SelectedObjects [0];
		SelLastObj 		= (SelectedObjects.Count == 0 ) ? null : SelectedObjects [SelectedObjects.Count -1];

		//Debug.Log (">> First Obj " + SelFirstObj + "  Last obj : " + SelLastObj);
		
		newR 	= (SelLastObj != null) ? SelLastObj.RowID : 0;
		newC 	= (SelLastObj != null) ? SelLastObj.ColID : 0;
		
		foreach(BaseBoardObject selObj in SelectedObjects)
		{
			SelAllCardObj &= selObj.CompareTag(C.CARDOBJ);
			SelAllSuitObj &= selObj.CompareTag(C.SUITOBJ);
			SelAllCoinObj &= selObj.CompareTag(C.COINOBJ);


			if(selObj.CompareTag(C.CARDOBJ) || selObj.CompareTag(C.SUITOBJ))
				SelAllSameSuit &= selObj.SameSuit(SelFirstObj);
			else
				SelAllSameSuit = false;

			if(selObj.CompareTag(C.CARDOBJ) && ((CardObject)selObj).Card != null)
			{
				SelCardCount += 1;
				SelAllCardVal += ((CardObject)selObj).Card.Num;
			}
			else
			{
				SelAllCardVal += 1;
			}
		}

		if(Game.Instance.CardLoopAround && SelAllCardVal > DeckData.MAXCARDS)
		{
			SelAllCardVal = SelAllCardVal - DeckData.MAXCARDS;
		}
	}
	
	void DestroyNRemoveFromBoard(BaseBoardObject obj, float time = 0.0f)
	{
		if(obj == null)
			return;

		if(Game.Instance.IsFinished)
			return;

		// Add the Burnt card back to Deck if necessary
		if(Game.Instance.ReuseBurntCard && obj != null && obj.CompareTag(C.CARDOBJ))
		{
			Game.Instance.CurrDeck().AddCardToDeck(((CardObject)obj).Card);
		}

		obj.MarkDirty();

		AllBoardObjects.Remove(obj);

		if(obj != null)
			Destroy(obj.gameObject,time);
		
		UpdateNeighboursNCardCount();
	}
	
	BaseBoardObject BoardObjectAt(int row, int col)
	{
		BaseBoardObject returnObj = null;
		
		foreach(BaseBoardObject _obj in AllBoardObjects)
		{
			if(_obj.RowID == row && _obj.ColID == col)
			{
				returnObj = _obj;
				break;
			}
		}
		
		return returnObj;
	}

	void ShowZippedPreview()
	{
		if(SelectedObjects.Count >= C.MIN_MIX_SEL &&  SelAllSameSuit && !SelAllCardObj)
		{
			if(SelLastObj != null && (SelLastObj.CompareTag(C.CARDOBJ) || (SelLastObj.CompareTag(C.SUITOBJ))))
			{
				SelLastObj.UpdatePreview(SelAllCardVal,SelLastObj.Suit);
				//previewCard.SetCardInfo(SelAllCardVal, SelLastObj.Suit);
			}
		}

		//if(SelAllCardObj)
		//	ShowSelFormedFeedback ();
	}

	void AddToBoardObjects(BaseBoardObject _card)
	{
		AllBoardObjects.Add (_card);
		UpdateNeighboursNCardCount();
	}

	void AddToSelectedList(BaseBoardObject _selObj, bool updateSelInfo = true)
	{
		_selObj.Select ();
		SelectedObjects.Add (_selObj);

		if(updateSelInfo)
			UpdateSelObjInfo ();
		
		//Clean the ClickCount  for Selected Items, if more than one item is Added
		if(SelectedObjects.Count > 1)
		{
			foreach(BaseBoardObject obj in SelectedObjects)
			{
				obj.ClickCount = 0;
				obj.ResetPreview();
			}
		}

		//SoundMgr.Instance.PlaySelectSound (SelectedObjects.Count);
		
		ShowZippedPreview ();
	}
	
	void RemoveFromSelectedObject(BaseBoardObject _card, bool updateSelInfo = true)
	{
		if(_card != null)
		{
			_card.DeSelect ();
			_card.ResetPreview();
		}

		SelectedObjects.Remove(_card);

		if(updateSelInfo)
			UpdateSelObjInfo ();

		ShowZippedPreview ();
	}
	
	public void ClearSelectedObjects()
	{
		foreach(BaseBoardObject _obj in SelectedObjects)
		{
			_obj.DeSelect();
			_obj.ResetPreview();
		}
		
		SelectedObjects.Clear ();
		UpdateSelObjInfo ();

		FeedbackContainer.Instance.HideContainer ();
		ShowZippedPreview ();
	}
	
	bool MatchesTagAndSuit(BaseBoardObject selObj)
	{
		bool returnVal = false;

		if(selObj.CompareTag(C.CARDOBJ) || selObj.CompareTag(C.SUITOBJ))
		{
			// We already have two objects in the list
			if(SelAllCardObj)
			{
				return (SelLastObj.CompareTag(selObj.tag));
			}
			else if(SelAllSuitObj && SelLastObj != null)
			{
				if(SelAllSameSuit)
					return (SelFirstObj.SameSuit(selObj));
				else
					return (SelLastObj.SameSuit(selObj) && SelFirstObj.SameSuit(selObj));
			}
			else if(SelAllSameSuit)// && SelCardCount == 0)
			{
				// This is a combination of Suits and Cards
				if(SelFirstObj.CompareTag(C.CARDOBJ))
					return (SelLastObj.SameSuit(selObj) && SelLastObj.CompareTag(selObj.tag));
				else
					return (SelLastObj.SameSuit(selObj));
			}
		}
		else if(selObj.CompareTag(C.COINOBJ))
		{
			return (SelLastObj.CompareTag(selObj.tag));
		}

		return returnVal;
	}

	public void ValidateAndAddToSelected(BaseBoardObject _selObj)
	{
		int selMax					= Game.GridRows * Game.GridColumns;
		bool isNeighbour 			= false;
		//bool isValidSuit  			= false;

		if(SelectedObjects.Contains (_selObj))
		{
			Debug.Log(">> 1");
			return;
		}

		//Neighbour & Diagonal Check
		if(SelectedObjects.Count >0)
		{
			if(SelLastObj.CompareTag(C.CARDOBJ))
				isNeighbour		= SelLastObj.IsNeighbour(_selObj,Game.Instance.AllowDiagCards);
			else
				isNeighbour		= SelLastObj.IsNeighbour(_selObj,Game.Instance.AllowDiagSuites);

			if(!isNeighbour)
			{
				//Debug.Log(">> 2");
				return;
			}
		}
		
		if(SelectedObjects.Count == 0 || (SelectedObjects.Count == 1))// && isNeighbour))
		{
			AddToSelectedList(_selObj);
		}
		else
		{
			if(SelAllCardObj && _selObj.CompareTag(C.CARDOBJ))
			{
				selMax = Game.Instance.FullHandValid ? C.FULL_HAND_SIZE: C.MINI_HAND_SIZE;
			}else
			{
				selMax        			= Game.GridRows * Game.GridColumns;
			}

			if(SelectedObjects.Count >= selMax)
			{
				//Debug.Log("Too many Cards selected");
				FeedbackContainer.Instance.ShowErrorFeedback("Too many card selected");
				Debug.Log(">> 3");
				return;
			}

			// Final ckeck
			if (MatchesTagAndSuit(_selObj))
			{
				AddToSelectedList(_selObj);
			}else
			{
				//Debug.Log("Invalid selection");
				Debug.Log(">> 4");
				FeedbackContainer.Instance.ShowErrorFeedback("Invalid selection");
				//SoundMgr.Instance.PlayVoiceFeedback(Enums.FEEDBACK_SND.NO);
			}
		}
	}

	public CardObject CardOnBoardWith(Enums.SUIT suitID, int cNum)
	{
		CardObject onBoardCard = null;
		
		foreach(BaseBoardObject obj in AllBoardObjects)
		{
			if(obj.CompareTag(C.CARDOBJ))
			{
				CardObject cObj = (CardObject)obj;
				
				if(cNum == cObj.Card.Num && suitID == cObj.Card.Suit)
				{
					onBoardCard = cObj;
					break;
				}
			}
		}
		
		return onBoardCard;
	}
	#endregion


	public void ShowSelFormedFeedback()
	{
		//		if(handPF != null)
		//		{
		//			handPF.UpdateWithCardObjs(GameBoard.Instance.SelectedObjects);
		//		}
		
		foreach(BaseBoardObject pObj in selectHandList)
		{
			Destroy(pObj.gameObject,0.1f);
		}
		selectHandList.Clear ();
		
		float scaleDiff = 0.75f;
		float posDiff	= 0.65f;
		if(GameBoard.Instance.SelectedObjects.Count > C.FULL_HAND_SIZE)
		{
			float diff 	= GameBoard.Instance.SelectedObjects.Count - C.FULL_HAND_SIZE;
			scaleDiff 	= 0.75f - (diff * 0.15f); 
			posDiff		= 0.65f - (diff * 0.15f);
		}
		
		int i = 0;
		foreach(BaseBoardObject obj in GameBoard.Instance.SelectedObjects)
		{
			BaseBoardObject newObj = null;
			
			if(obj.CompareTag(C.CARDOBJ))
			{
				newObj = PFLoader.Instance.CardPrefabObj;
				((CardObject)newObj).SetCardData(((CardObject)obj).Card);
			}
			else if(obj.CompareTag(C.SUITOBJ))
			{
				newObj = PFLoader.Instance.SuitsPrefabObj;
				newObj.Suit = obj.Suit;
			}
			
			selectHandList.Add(newObj);
			
			newObj.transform.position 		= selCardHolder.transform.position + new Vector3(i* posDiff, 0, 0);
			newObj.transform.localScale		= new Vector3(scaleDiff,scaleDiff,1.0f);
			i++;
		}
	}
}
