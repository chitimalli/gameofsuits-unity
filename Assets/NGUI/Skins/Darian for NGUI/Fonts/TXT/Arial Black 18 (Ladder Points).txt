info face=Arial Black 18 (Ladder Points size=18 bold=0 italic=0 charset= unicode= stretchH=100 smooth=1 aa=1 padding=2,2,2,2 spacing=0,0 outline=0
common lineHeight=25 base=20 scaleW=130 scaleH=18 pages=1 packed=0
page id=0 file="Arial Black 18 (Ladder Points.png"
chars count=10
char id=48 x=2 y=2 width=11 height=14 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=49 x=15 y=2 width=8 height=13 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=50 x=25 y=2 width=11 height=13 xoffset=0 yoffset=0 xadvance=12 page=0 chnl=15
char id=51 x=38 y=2 width=11 height=14 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=52 x=51 y=2 width=12 height=13 xoffset=0 yoffset=0 xadvance=12 page=0 chnl=15
char id=53 x=65 y=2 width=11 height=13 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=54 x=78 y=2 width=11 height=14 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=55 x=91 y=2 width=11 height=13 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=56 x=104 y=2 width=11 height=14 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=57 x=117 y=2 width=11 height=14 xoffset=1 yoffset=0 xadvance=12 page=0 chnl=15
char id=32 x=0 y=0 width=0 height=0 xoffset=1 yoffset=0 xadvance=6 page=0 chnl=15
kernings count=0