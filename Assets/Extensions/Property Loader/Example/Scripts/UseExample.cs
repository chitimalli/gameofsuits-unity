////////////////////////////////////////////////////////////////////////////////
//  
// @module Unity3d Property / Config System
// @author Osipov Stanislav lacost.st@gmail.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UseExample : MonoBehaviour {
	
	private Rect buttonRect;
	private Rect wbuttonRect;
	
	private Rect labelRect;
	private GUIStyle labelStyle;

	//--------------------------------------
	// INITIALIZE
	//--------------------------------------
	
	void Awake() {
		buttonRect =  new Rect((Screen.width - 150) / 2  + 150, ExampleConfig.RELOAD_BUTTON_POS, 160, 60);
		wbuttonRect =  new Rect((Screen.width - 150) / 2 - 150, ExampleConfig.RELOAD_BUTTON_POS, 160, 60);
		
		labelRect  = new Rect(0, ExampleConfig.TEXT_POS, Screen.width, 100);
		
		labelStyle =  new GUIStyle();
		labelStyle.fontSize = 56;
		labelStyle.alignment = TextAnchor.MiddleCenter;
		labelStyle.normal.textColor = Color.white;
		
		if(ExampleConfig.BOOL_TEST) {
			Debug.Log("bool test is true");
		} else {
			Debug.Log("bool test is false");
		}

	}

	//--------------------------------------
	//  PUBLIC METHODS
	//--------------------------------------
	
	void OnGUI() {
		if(GUI.Button(buttonRect, "Reload from Resources")) {
			Properties.reload(PropertyFile.GAME_PLAY);
		}
		
		if(GUI.Button(wbuttonRect, "Reload via WWW")) {
			PropertyLoader loader = PropertyLoader.Create();
			loader.onLoadComplete += onLoad;
			loader.load(ExampleConfig.EXTARNAL_CONFIG_PATH, PropertyFile.GAME_PLAY);
		}
		
		GUI.Label(labelRect, ExampleConfig.WHO + " eats " + ExampleConfig.HOW_MANY.ToString() + " " +  ExampleConfig.WHAT, labelStyle);
		
	}
	
	
	//--------------------------------------
	//  GET/SET
	//--------------------------------------
	
	//--------------------------------------
	//  EVENTS
	//--------------------------------------
	
	private void onLoad() {
		Debug.Log("Game Play Properties reloaded from www");
	}
	
	//--------------------------------------
	//  PRIVATE METHODS
	//--------------------------------------
	
	//--------------------------------------
	//  DESTROY
	//--------------------------------------


}
