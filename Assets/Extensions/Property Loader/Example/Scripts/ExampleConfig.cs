////////////////////////////////////////////////////////////////////////////////
//  
// @module Unity3d Property / Config System
// @author Osipov Stanislav lacost.st@gmail.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExampleConfig  {
	
	
	public static string BASE_URL {
		get {
			switch(Application.platform) {
				case RuntimePlatform.Android:
					return "jar:file://" + Application.dataPath + "!/assets/";
				case RuntimePlatform.IPhonePlayer:
					return "file://" + Application.dataPath + "/Raw/";
				default:					
					return "file://" + Application.dataPath + "/StreamingAssets/";
			}
		}
	}
	
	public static string EXTARNAL_CONFIG_PATH {
		get {
			return BASE_URL + Properties.getString(PropertyFile.GAME_PLAY, "extarnal_config_path");
		}
	} 
	
	
	

	public static string WHO {
		get {
			return Properties.getString(PropertyFile.GAME_PLAY, "who");
		}
	}
	
	
	public static string WHAT {
		get {
			return Properties.getString(PropertyFile.GAME_PLAY, "what");
		}
	}
	
	
	public static int HOW_MANY {
		get {
			return Properties.getInt(PropertyFile.GAME_PLAY, "how_many");
		}
	}
	
	
	public static float RELOAD_BUTTON_POS {
		get {
			return Properties.getFloat(PropertyFile.GAME_PLAY, "reload_button_pos");
		}
	}
	
	
	public static float TEXT_POS {
		get {
			return Properties.getFloat(PropertyFile.GAME_PLAY, "text_pos");
		}
	}
	
	public static bool BOOL_TEST {
		get {
			return Properties.getBool(PropertyFile.GAME_PLAY, "bool_test");
		}
	}

	
	

}
