////////////////////////////////////////////////////////////////////////////////
//  
// @module Unity3d Property / Config System
// @author Osipov Stanislav lacost.st@gmail.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PropertyLoader : MonoBehaviour {
	
	public delegate void PropertyLoaderEventDelegate();
	
	public PropertyLoaderEventDelegate onLoadComplete;
	public PropertyLoaderEventDelegate onLoadFailed;
	
	
	private string _fileName;
	
	
	//--------------------------------------
	// INITIALIZE
	//--------------------------------------
	
	public static PropertyLoader Create() {
		return new GameObject("PropertyLoader").AddComponent<PropertyLoader>();
	}


	//--------------------------------------
	//  PUBLIC METHODS
	//--------------------------------------
	
	public void load(string path, string flieName) {
		_fileName = flieName;
  		WWW www = new WWW (path + flieName + ".txt");
	    StartCoroutine(WaitForRequest (www));
    }
	
	//--------------------------------------
	//  GET/SET
	//--------------------------------------
	
	//--------------------------------------
	//  EVENTS
	//--------------------------------------
	
	//--------------------------------------
	//  PRIVATE METHODS
	//--------------------------------------
	
	private IEnumerator WaitForRequest(WWW www) {
        yield return www;

        // check for errors
        if (www.error == null) {
			
			Properties.setFile(_fileName, www.text);
			if(onLoadComplete != null) {
				onLoadComplete();
			}
        } else {
			if(onLoadFailed != null) {
				onLoadFailed();
			}
            Debug.LogWarning("PropertyLoader Error: "+ www.error);
			Destroy(gameObject);
        }
	}
	
	//--------------------------------------
	//  DESTROY
	//--------------------------------------


}
