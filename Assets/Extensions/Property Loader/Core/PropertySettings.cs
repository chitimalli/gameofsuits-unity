////////////////////////////////////////////////////////////////////////////////
//  
// @module Unity3d Property / Config System
// @author Osipov Stanislav lacost.st@gmail.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PropertySettings  {
	
	public static string GENERAL_PATH = "Properties/";
	public static string DEFAULT_CONFIG = PropertyFile.DEFAULT;
}
