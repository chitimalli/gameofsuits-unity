////////////////////////////////////////////////////////////////////////////////
//  
// @module Unity3d Property / Config System
// @author Osipov Stanislav lacost.st@gmail.com
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Properties  {
	
	public static Dictionary<string, Dictionary<string, string>> files =  new Dictionary<string, Dictionary<string, string>>();

	//--------------------------------------
	// INITIALIZE
	//--------------------------------------
	
	public static void setFile(string fileName, string data) {
		parcePropertyFile(fileName, data);
	}

	//--------------------------------------
	//  PUBLIC METHODS
	//--------------------------------------
	
	public static string getString(string property) {
		return getString(PropertySettings.DEFAULT_CONFIG, property);
	}
	
	public static string getString(string file, string property) {
		chackForFile(file);
		return files[file][property];
	}
	
	public static int getInt(string property) {
		return getInt(PropertySettings.DEFAULT_CONFIG, property);
	}
	
	public static int getInt(string file, string property) {
		chackForFile(file);
		return System.Convert.ToInt32(files[file][property]);
	}
	
	
	public static float getFloat(string property) {
		return getFloat(PropertySettings.DEFAULT_CONFIG, property);
	}
	
	public static float getFloat(string file, string property) {
		chackForFile(file);
		return System.Convert.ToSingle(files[file][property]);
	}
	
	public static bool getBool(string property) {
		return getBool(PropertySettings.DEFAULT_CONFIG, property);
	}
	
	public static bool getBool(string file, string property) {
		chackForFile(file);
		return System.Convert.ToBoolean(files[file][property]);
	}
	
	
	
	
	public static void reload(string file) {
		loadPropertyFile(file);
	}
	
	
	//--------------------------------------
	//  GET/SET
	//--------------------------------------
	
	//--------------------------------------
	//  EVENTS
	//--------------------------------------
	
	//--------------------------------------
	//  PRIVATE METHODS
	//--------------------------------------
	
	private static void chackForFile(string file) {
		if(!files.ContainsKey(file)) {
			loadPropertyFile(file);
		}
		
	}
	
	private static void loadPropertyFile(string file) {
		TextAsset prop = Resources.Load(PropertySettings.GENERAL_PATH + file) as TextAsset;
		
		if(prop == null) {
			return;
		}
		
		parcePropertyFile(file, prop.text);
	}
	
	private static void parcePropertyFile(string file, string data) {
		if(files.ContainsKey(file)) {
			files[file] = new Dictionary<string, string>();
		} else {
			files.Add(file, new Dictionary<string, string>());
		}
		
		
		string sub;
		string text =  deleteComents(data);
		
		while(text.IndexOf("\n") != -1 && text.IndexOf(":") != -1) {
			sub = text.Substring(0, text.IndexOf("\n"));
			parcePair(file, sub);
			
			int start = text.IndexOf("\n") + 1;
			int len = text.Length - start;
			text = text.Substring(start, len);
		}
		
		if(text.IndexOf(":") != -1) {
			parcePair(file, text);
		}
	}
	
	
	private static void parcePair(string file, string pair) {
		
		int dotIndex = pair.IndexOf(":");
		
		if(dotIndex == -1) {
			return;
		}
		
		string key = pair.Substring(0, dotIndex);
		string val = pair.Substring(dotIndex + 1, pair.Length - dotIndex - 1);
		
		val = val.Replace("\\", "");
		files[file].Add(key.Trim(), val.Trim());
	}
	
	private static string deleteComents(string text) {
		
		text = deletePatern(text, "//", "\n");
		text = deletePatern(text, "/*", "*/");
		
		return text;
	}
	
	private static string deletePatern(string text, string startPatern, string endPatern) {
		string newtext;
		
		while(text.IndexOf(startPatern) != -1) {
			int signle = text.IndexOf(startPatern);
			
			
			newtext = text.Substring(0, signle);
			
			string nextSub = text.Substring(signle, text.Length - signle);
			int end = nextSub.IndexOf(endPatern);
			if(end != -1) {
				newtext += nextSub.Substring(end, nextSub.Length - end);
			}
			
			text = newtext;
		}
		
		return text;
	}
	
	//--------------------------------------
	//  DESTROY
	//--------------------------------------


}
