﻿using System.Collections;
using Assets.Plugins.Drop3DEffects.Scripts;
using UnityEngine;

namespace Assets.Plugins.Drop3DEffects.Demo
{
	public class DemoScript : MonoBehaviour
	{
		private const int ButtonWidth = 100;
		private const int ButtonHeight = 25;

		private int _selectedPrefabInd;
		private int _selectedAnimationInd;
		private int _objectsCount;
		private float _duration;
		private float _speed;
	    private bool _isStartupRunning;

		public Transform[] Models;
		public Animator3D[] Animations;
        public float AnimationDuration = 2.0f;

		public void Start()
		{
		    _isStartupRunning = true;
			BindSettings();
		    StartCoroutine(StartStartupRunning());
		}

	    private IEnumerator StartStartupRunning()
	    {
	        for (;;)
	        {
	            if (!_isStartupRunning)
	                break;

	            Launch();
	            _selectedPrefabInd = (_selectedPrefabInd + 1)%Models.Length;
	            _selectedAnimationInd = (_selectedAnimationInd + 1)%Animations.Length;
	            BindSettings();
	            yield return new WaitForSeconds(AnimationDuration);
	        }
	    }

		public void OnGUI()
		{
		    if (_isStartupRunning)
		    {
                if (GUI.Button(new Rect(
                Screen.width / 2 - 180 / 2,
                Screen.height - 40 - 5,
                180, 40), "Stop"))
                {
                    _isStartupRunning = false;
                }	
		    }
		    else
		    {
                ShowSettings();
                ShowSelectAnimationGUI();
                ShowSelectModelGUI();
                ShowLaunchButton();   
		    }
		}

		private void ShowSettings()
		{
			GUILayout.BeginVertical();
			GUILayout.BeginHorizontal();
			GUILayout.Box(string.Format("Objects count: {0}", _objectsCount.ToString("000")));
			GUILayout.Space(10);
			_objectsCount = (int) GUILayout.HorizontalSlider(_objectsCount, 1, 500, GUILayout.Width(200));
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			GUILayout.Box(string.Format("Duration: {0}", _duration.ToString("F")));
			GUILayout.Space(10);
			_duration = GUILayout.HorizontalSlider(_duration, 0, 60, GUILayout.Width(200));
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			GUILayout.Box(string.Format("Speed: {0}", _speed.ToString("F")));
			GUILayout.Space(10);
			_speed = GUILayout.HorizontalSlider(_speed, 0.0f, 10.0f, GUILayout.Width(200));
			GUILayout.EndHorizontal();

			GUILayout.EndVertical();
		}

		private void Launch()
		{
			Animator3D anim = Animations[_selectedAnimationInd];
			anim.ObjectPrefab = Models[_selectedPrefabInd];
			anim.StartSpeed = _speed;
			anim.Duration = _duration;
			anim.Count = _objectsCount;
			anim.Run();
		}

		private void BindSettings()
		{
			Animator3D anim = Animations[_selectedAnimationInd];
			_speed = anim.StartSpeed;
			_duration = anim.Duration;
			_objectsCount = anim.Count;			
		}

		private void ShowSelectModelGUI()
		{
			if (GUI.Button(new Rect(
				Screen.width / 2 - ButtonWidth / 2 - 150,
				Screen.height - ButtonHeight - 50,
				ButtonWidth, ButtonHeight), "<<<"))
			{
				SelectPreviouseModel();
			}

			GUI.Box(new Rect(
				Screen.width/2 - 180/2,
				Screen.height - ButtonHeight - 50,
				180, ButtonHeight),
				string.Format("Model: \"{0}\"", Models[_selectedPrefabInd].name));

			if (GUI.Button(new Rect(
				Screen.width / 2 - ButtonWidth / 2 + 150,
				Screen.height - ButtonHeight - 50,
				ButtonWidth, ButtonHeight), ">>>"))
			{
				SelectNextModel();
			}			
		}

	    private void SelectPreviouseModel()
	    {
            _selectedPrefabInd = (Models.Length + _selectedPrefabInd - 1) % Models.Length;
            Launch();
	    }

	    private void SelectNextModel()
	    {
            _selectedPrefabInd = (_selectedPrefabInd + 1) % Models.Length;
            Launch();
	    }

		private void ShowSelectAnimationGUI()
		{
			if (GUI.Button(new Rect(
				Screen.width / 2 - ButtonWidth / 2 - 150,
				Screen.height - ButtonHeight - 80,
				ButtonWidth, ButtonHeight), "<<<"))
			{
                SelectPreviouseAnimation();
			}

			GUI.Box(new Rect(
				Screen.width / 2 - 180 / 2,
				Screen.height - ButtonHeight - 80,
				180, ButtonHeight),
				string.Format("Animation: \"{0}\"", Animations[_selectedAnimationInd].name));

			if (GUI.Button(new Rect(
				Screen.width / 2 - ButtonWidth / 2 + 150,
				Screen.height - ButtonHeight - 80,
				ButtonWidth, ButtonHeight), ">>>"))
			{
				SelectNextAnimation();
			}
		}

	    private void SelectNextAnimation()
	    {
            _selectedAnimationInd = (_selectedAnimationInd + 1) % Animations.Length;
            BindSettings();
            Launch();
	    }

	    private void SelectPreviouseAnimation()
	    {
            _selectedAnimationInd = (Animations.Length + _selectedAnimationInd - 1) % Animations.Length;
            BindSettings();
            Launch();
	    }

		private void ShowLaunchButton()
		{
			if (GUI.Button(new Rect(
				Screen.width / 2 - 180 / 2,
				Screen.height - 40 - 5,
				180, 40), "Run"))
			{
				Launch();
			}	
		}

	    public void Update()
	    {
	        if (!_isStartupRunning)
	        {
	            if (Input.GetKeyUp(KeyCode.LeftArrow))
	            {
	                SelectPreviouseModel();
	            }
	            else if (Input.GetKeyUp(KeyCode.RightArrow))
	            {
	                SelectNextModel();
	            }
	            else if (Input.GetKeyUp(KeyCode.UpArrow))
	            {
	                SelectPreviouseAnimation();
	            }
	            else if (Input.GetKeyUp(KeyCode.DownArrow))
	            {
	                SelectNextAnimation();
	            }
	            else if (Input.GetKeyUp(KeyCode.Space))
	            {
	                Launch();
	            }
	        }
	    }

	}
}
